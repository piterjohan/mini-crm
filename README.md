# How To Use

Here the step using mini crm step

1. clone or download this file
2. > composer install
3. Auth laravel

    > composer require laravel/ui:^2.4

    > php artisan ui bootstrap --auth

    > npm install & npm run dev

4. copy .env.example to .env
5. edit file .env like below

```
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT= your port
    DB_DATABASE= your databasename
    DB_USERNAME= your username
    DB_PASSWORD= your password
```

6. Then for using email we need setup [link to mailtrap.io!](hhttps://mailtrap.io/)
7. edit .env like below

```
    MAIL_MAILER=smtp
    MAIL_HOST=smtp.mailtrap.io
    MAIL_PORT=2525
    MAIL_USERNAME= your username mailtrap
    MAIL_PASSWORD= your password mailtrap
    MAIL_ENCRYPTION=tls
```

8. Then if didnt reach the mail.io try to setup like below
    > config/mail.php

```
    'mailers' => [
            'smtp' => [
                'transport' => 'smtp',
                'host' => env('MAIL_HOST', 'smtp.mailtrap.io'),
                'port' => env('MAIL_PORT', 2525),
                'encryption' => env('MAIL_ENCRYPTION', 'tls'),
            ],
```

9. To link store
    > php artisan storage:link
10. Migrate databse
    > php artisan:migrate
11. Seeding
    > php artisan db:seed
12. For notification queuing email this project use notificationEmailCompany for queue email
    > php artisan queue:work --queue=notificationEmailCompany

The application ready to go

# API Support

## 1. Get Token by login

> api/auth/v1/login

### Paramater

1. email
2. password

### Response

```
    {
        "access_token": your_token,
        "token_type": "Bearer"
    }
```

## 2. Get List of Employees by token & Company Id

> api/auth/v1/company-employees

### Header

1. Bearer 'Your Token'

### Paramater

1. company_id

### Response

```
    {
        "message": "success",
        "data": [
             {
            "some attribute": "value",
            "company": {
                "some attribute": "value",
                }
            },
        ],

    }
```
The API application ready to go

