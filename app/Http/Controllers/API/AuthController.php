<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\CompaniesModel;
use App\Models\EmployeesModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Request token
     * @param string $request->company_id
     * @return json 
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        try {
            // check password
            $credentials = request(['email', 'password']);

            $response = (Auth::attempt($credentials)) ? $this->generateToken($request)  : $this->errorResponse();

            return $response;
        } catch (\Throwable $th) {
            return response()->json(['error' => 'Please check email & password'], 401);
        }
    }

    /**
     * Generate Token
     * @param request $request
     * @return response json
     */
    protected function generateToken($request)
    {
        // find user
        $user = User::where('email', $request->email)->first();

        //delete old token
        $user->tokens()->where('name', $user->email)->delete();
        // renew token
        $token = $user->createToken($user->email)->plainTextToken;
        // $client_token = $user->tokens[0]->token;

        return response()->json(['access_token' => $token, 'token_type' => 'Bearer',], 200);
        // return response()->json(['access_token' => $token, 'client_token' => $client_token,'token_type' => 'Bearer',], 200);
    }

    /**
     * @return response json
     * message
     */
    protected function errorResponse()
    {
        return response()->json(['message' => 'Please check email & password'], 401);
    }

    /**
     * Get data by token header
     */

    public function companyEmployees(Request $request)
    {
        $request->validate([
            'company_id' => 'required',
        ]);

        $employees = EmployeesModel::with('company')->where('id_company', $request->company_id)->get();

        return response()->json(['message' => 'success', 'data' => $employees], 200);
    }
}
