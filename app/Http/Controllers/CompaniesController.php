<?php

namespace App\Http\Controllers;

use App\Imports\CompaniesImport;
use App\Jobs\SendEmailCompanies;
use App\Models\CompaniesModel;
use App\Http\Requests\Companies\ValidateCreateCompanies;
use App\Http\Requests\Companies\ValidateImportCompanies;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class CompaniesController extends Controller
{
    public function getLanguage()
    {
        $lang = app()->getLocale();
        return $lang;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company_data = CompaniesModel::orderByDesc('id_company')->get();

        return view('admin-panel/show/companies-data',[
            'companies' => true,
            'indexCompanies' => true,
            'company_data' => $company_data,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin-panel/input_data/create_company',[
            'companies' => true,
            'crateCompanies' => true,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateCreateCompanies $request)
    {
        DB::beginTransaction();
        try {
            // if logo didnt null
            if($request->logo != null){
                //get name from storage
                $logo_name = $this->saveLogoCompany($request);
                // save DB, and get idCompany
                $id_company = $this->saveDataCompany($request, $logo_name);
                // send mail
                $this->sendNotificationCompany($id_company);
            }

            // if logo null
            if($request->logo == null)
            {
                // save DB, and get idCompany
                $id_company = $this->saveDataCompany($request, null);
                // send mail
                $this->sendNotificationCompany($id_company);
            }
            DB::commit();
            return redirect()->route('companies.index', app()->getLocale())->with('success', 'Company Created');
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->route('companies.index', app()->getLocale())->with('error', 'Maintance');
        }

    }

    /**
     *  function company for save logo
     * @param request $data
     * @return Storage $path
     * */ 

    private function saveLogoCompany($data)
    {
        // save photo
        $image = $data->file('logo');
        $path = Storage::putFile('public/',$image);
        return $path;
    }

    /**
     * function company save data
     * @param request $data
     * @method storage $path
     */
    private function saveDataCompany($data, $path)
    {
        $create_company = CompaniesModel::create([
            'name' => ucwords($data->name),
            'email' => strtolower($data->email),
            'logo' => (pathinfo($path)['basename']) ? pathinfo($path)['basename'] : "" ,
            'created_at' => Carbon::now(),
            'created_by_id' => Session::get('jwt_data')->id,
        ]);

        $get_id_company = $create_company->id_company;
        return $get_id_company;
    }

    // Send email notification
    private function sendNotificationCompany($id_company)
    {
        $data_company = CompaniesModel::find($id_company);
        // instend of send better queuing
        SendEmailCompanies::dispatch($data_company->email)->onQueue('notificationEmailCompany');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $title = "Edit Data Company";
        $company = CompaniesModel::find($id);

        return view('admin-panel/edit_data/edit-company-data',[
            'companies' => true,
            'crateCompanies' => true,
            'title' => $title,
            'company' => $company,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateCreateCompanies $request, $locale, $id)
    {   
        DB::beginTransaction();
        try {
            if ( empty($request->logo) )
            {
                // find company
                $old_image = CompaniesModel::find($id);
                // update data company
                $this->updateDataCompany($request, $id, null, false, $old_image->logo);
            }

            if ($request->logo)
            {
                // Delete old Logo
                $this->deleteOldLogoCompanyForUpdate($id);
                // get new logo name
                $newLogoName = $this->saveLogoCompany($request);
                // update data company
                $this->updateDataCompany($request,$id, $newLogoName);
            }
            
            DB::commit();
            return redirect()->route('companies.index', app()->getLocale())->with('success', 'Company Have Been Updated');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->route('companies.index', app()->getLocale())->with('error', 'Maintance');
        }
        
    }

    /**
     *  delete old logo company for update
     * @param string $id
     * */ 
    private function deleteOldLogoCompanyForUpdate($id)
    {
        $data_company = CompaniesModel::find($id);
        Storage::disk('public')->delete($data_company->logo);
    }

    /**
     * delete old logo company byName
     * @param string $logo_name
     * */
    private function deleteOldLogoCompanyByName($logo_name)
    {
        Storage::disk('public')->delete($logo_name);
    }

    /**
     * update data company
     * @param request $data
     * @param string $id id_company
     * @method $path get from return storage
     * @param bool $old_logo_name used if $is_from_storage = false
     * @param bool $old_logo_name used if $is_from_storage = true $old_logo_name can be ignore
     * */
    private function updateDataCompany($data, $id, $path = null, $is_from_storage = true, $old_logo_name = null)
    {
        CompaniesModel::where('id_company', $id)
        ->update([
            'name' => ucwords($data->name),
            'email' => strtolower($data->email),
            'logo' => ($is_from_storage) ? pathinfo($path)['basename'] : $old_logo_name,
            'updated_at' => Carbon::now(),
            'updated_by_id' => Session::get('jwt_data')->id,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {

        DB::beginTransaction();
        try {
            
            // find data before delete
            $company    = CompaniesModel::find($id);
            // check before delete
            $is_deleted = CompaniesModel::destroy($id);
            // if deleted also delete storage img
            ($is_deleted) ?  $this->deleteOldLogoCompanyByName($company->logo) : ""; 

            DB::commit();
            return redirect()->route('companies.index', app()->getLocale())->with('success', 'Company Have Been Deleted');
        } catch (\Throwable $th) {
            DB::rollback();
            if($this->getLanguage() == "en")
            {
                return redirect()->route('companies.index', app()->getLocale())->with('error', 'Please Delete Data Employee Who Belong This Company');
            }else{
                return redirect()->route('companies.index', app()->getLocale())->with('error', 'Delete dulu data yang karyawan yang berelasi dengan perusahaan');
            }
        }
    }

    // Import Companies
    public function importCompanies(ValidateImportCompanies $request)
    {
        try {
             // Import Data Companies
            Excel::import(new CompaniesImport, $request->data_company);
            // save data excel
            $this->saveImportDataCompany($request);
            return redirect()->route('companies.index', app()->getLocale())->with('success', 'Import Data Company Success');

        } catch (\Throwable $th) {
            return redirect()->route('companies.index', app()->getLocale())->with('error', 'Maintance');
        }
       
    }

    /**
     * function company for save import excel
     * @param request $data
     * @return name base on pathinfo($path)['basename']
     */
    private function saveImportDataCompany($data)
    {
        // save photo
        $file = $data->file('data_company');
        $path = Storage::putFile('import-excel/',$file);
        return pathinfo($path)['basename'];
    }
}