<?php

namespace App\Http\Controllers;

use App\Models\CompaniesModel;
use App\Models\EmployeesModel;
use App\Models\ItemsModel;
use App\Models\SellsModel;
use App\Models\SellsSummaryModel;
use App\ServiceClass\SellsSummaryService;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;

class DataController extends Controller
{

    public function index()
    {

        return view('admin-panel/show.all-data', [
            'filter_data_menu' => true,
            'list_timezone' => $this->listTimeZone(),
        ]);
    }

    /**
     * Sells summary
     */
    public function sellSummary()
    {
    
        $employees = EmployeesModel::all();
        $companies = CompaniesModel::all();

        return view('admin-panel/show.sell-summary-daily', [
            'sells_menu' => true,
            'sells_summary_daily' => true,
            'employees' => $employees,
            'companies' => $companies,
        ]);
    }

    /**
     * Searching sell summary
     */
    public function searchingSellSummary(Request $request)
    {
        $this->guardSelectSection($request);

        try {

            $sell_summary_service = new SellsSummaryService();
            $get_summary = $sell_summary_service->filterSearchingSection($request);

            $employees = EmployeesModel::all();
            $companies = CompaniesModel::all();

            if ($get_summary[0] == "employee" || $get_summary[0] == "date") {
                return view('admin-panel/show.sell-summary-daily', [
                    'sells_menu' => true,
                    'sells_summary_daily' => true,
                    'sells_summary' => true,
                    'employees' => $employees,
                    'companies' => $companies,
                    'sells_summary_employee' => $get_summary[1],
                ]);
            } else {
                return view('admin-panel/show.sell-summary-daily', [
                    'sells_menu' => true,
                    'sells_summary_daily' => true,
                    'sells_summary' => true,
                    'employees' => $employees,
                    'companies' => $companies,
                    'sells_summary_company' => $get_summary[1],
                ]);
            }
        } catch (\Throwable $th) {
            dd($th);
            return redirect()->route('data.sellSummary', app()->getLocale())->with('error', 'Maintance');
        }
    }

    /**
     * Details Sell Summary
     */
    public function detailSellSummary($locale, $id)
    {

        $sell_summary_service = new SellsSummaryService();

        $sells_data = $sell_summary_service->getSells($id);

        return view('admin-panel/show.detail-sell-summary', [
            'sells_menu' => true,
            'sells_summary_daily' => true,
            'sells_data' => $sells_data,
        ]);
    }


    /**
     * searching company for menu filter data
     */
    public function searchingCompany(Request $request)
    {
        try {
            $data_searching = $request->searchingCompany;
            $date_format   = date("Y-m-d", strtotime($request->searchingCompanyByDate));
            $get_current_timezone = $request->timezone;

            $companies_data = CompaniesModel::Where("name", "like", $data_searching . "%")
                ->orWhere("email", "like", $data_searching . "%")
                ->orWhere("created_at", "like", ($date_format) ? date($date_format) . "%" : "")
                ->orderBy('id_company', 'DESC')
                ->paginate(3, ['*'], 'companyPage');

            $employees_data = EmployeesModel::orderBy('id_employee', 'DESC')
                ->paginate(2, ['*'], 'employeePage');

            return view('admin-panel/show.all-data', [
                'filter_data_menu' => true,
                'list_timezone' => $this->listTimeZone(),
                'companies_data' => $companies_data,
                'employees_data' => $employees_data,
                'get_current_timezone' => $get_current_timezone,
            ]);
        } catch (\Throwable $th) {
            return redirect()->route('data.index');
        }
    }

    /**
     * searching employee for menu filter data
     */
    public function searchingEmployee(Request $request)
    {
        $data_searching = $request->searchingEmployee;
        $date_format   = date("Y-m-d", strtotime($request->searchingEmployeeByDate));
        $get_current_timezone = $request->timezone;
        try {
            $employees_data = EmployeesModel::where("first_name", "like", $data_searching . "%")
                ->orwhere("last_name", "like", $data_searching . "%")
                ->orwhere("email", "like", $data_searching . "%")
                ->orwhere("phone", "like", $data_searching . "%")
                ->orWhere("created_at", "like", ($date_format) ? date($date_format) . "%" : "")
                ->orderBy("id_employee", "DESC")
                ->paginate(3, ['*'], 'employeePage');

            $companies_data = CompaniesModel::orderBy("id_company", "DESC")
                ->paginate(3, ['*'], 'companyPage');

            return view('admin-panel/show.all-data', [
                'filter_data_menu' => true,
                'companies_data' => $companies_data,
                'employees_data' => $employees_data,
                'list_timezone' => $this->listTimeZone(),
                'get_current_timezone' => $get_current_timezone,
            ]);
        } catch (\Throwable $th) {
            return redirect()->route('data.index');
        }
    }


    /**
     * get price for create sell
     * @param Request $request->item_id
     */
    public function getPrice(Request $request)
    {
        if ($request->ajax()) {
            $item = ItemsModel::Select('price')->find($request->item_id);
            return response()->json(['data' => $item]);
        }
    }

    /**
     * Guard Selection section at sell-summary
     * @param Request $request
     */
    protected function guardSelectSection($request)
    {
        if ($request->employee != null && $request->company != null) {

            if (app()->getLocale() == "en") {
                return redirect()->route('data.sellSummary', app()->getLocale())->with(['error' => 'Choose one between Employee & Company']);
            } else {
                return redirect()->route('data.sellSummary', app()->getLocale())->with(['error' => 'Pilih Salah Satu Antara Karyawan & Perusahaan']);
            }
        }
    }

    /**
     * list of time zone
     * convert from array to object
     * @return object
     * timezone_indentifiers_list code from 
     * https://www.w3schools.com/php/func_date_timezone_identifiers_list.asp
     */
    protected function listTimeZone()
    {
        // list of GMT
        $gmt = timezone_identifiers_list(2047);

        // convert to object 
        $object_gmt = [];
        foreach ($gmt as $key => $item_tz) {
            // current timezone
            $dtz = new DateTimeZone($item_tz);
            // time in current timezone
            $time_in_currentTZ = new DateTime('now', $dtz);
            $offset = $dtz->getOffset($time_in_currentTZ) / 3600;
            $get_gmt = "GMT" . ($offset < 0 ? $offset : "+" . $offset);
            // object
            $gmt[$key] = array_push($object_gmt, (object)[
                'timezone' => $item_tz,
                'GMT' => $get_gmt,
            ]);
        }

        return $object_gmt;
    }
}
