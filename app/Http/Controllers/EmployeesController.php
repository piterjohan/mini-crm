<?php

namespace App\Http\Controllers;

use App\Imports\EmployeeImport;
use App\Models\CompaniesModel;
use App\Models\EmployeesModel;
use Illuminate\Support\Facades\DB as DB;
use App\Http\Requests\Employees\ValidateCreateEmployees;
use App\Http\Requests\Employees\ValidateImportEmployee;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Data Employee";
        $dataEmployeesWithCompany = $this->dataEmployeeWithCompany();

        return view('admin-panel/show.employee-data',[
            'title' => $title,
            'employees_menu' => true,
            'indexEmployees' => true,
            'dataEmployeesWithCompany' => $dataEmployeesWithCompany,
        ]);
    }

    // Employees Model with company
    private function dataEmployeeWithCompany()
    {   
        $data_employees_with_company = EmployeesModel::orderBy('id_employee', 'desc')->get();

        return $data_employees_with_company;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // fetch data companies for selected form
        $data_companies = $this->dataCompanies();

        return view('admin-panel/input_data/create-employees',[
            'employees_menu' => true,
            'crateEmployees' => true,
            'data_companies' => $data_companies,

        ]);
    }

    // get data company
    private function dataCompanies()
    {
        $data_companies = CompaniesModel::orderBy('id_company', 'desc')->get();

        return $data_companies;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateCreateEmployees $request)
    {
        
        try {
            // Save Company
            EmployeesModel::create([
                'first_name' => ucwords($request->first_name),
                'last_name' => ucwords($request->last_name),
                'id_company' => $request->id_company,
                'email' => strtolower($request->email),
                'phone' => $request->phone,
                'password' => Hash::make($request->password),
                'created_at' => Carbon::now(),
                'created_by_id' => Session::get('jwt_data')->id,
            ]);
            
            return redirect()->route('employees.index', app()->getLocale())->with('success', 'Employee Created');
        } catch (\Throwable $th) {
            return redirect()->route('employees.index', app()->getLocale())->with('error', 'Maintance');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale ,$id)
    {
        
        $employee = EmployeesModel::find($id);
        // fetch data companies for selected form
        $dataCompanies = $this->dataCompanies();

        return view('admin-panel/edit_data.edit-employee-data',[
            'employees_menu' => true,
            'crateEmployees' => true,
            'dataCompanies' => $dataCompanies,
            'employee' => $employee,
        ]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateCreateEmployees $request, $locale, $id)
    {
        //
        try {
            $this->updateDataCompany($request, $id);
            return redirect()->route('employees.index', app()->getLocale())->with('success', 'Employee Have Been Updated');
        } catch (\Throwable $th) {
            return redirect()->route('employees.index', app()->getLocale())->with('error', 'Maintance');
        }

    }

    /**
     * updateData Company
     * @param request $data
     * @param string $id 
     * $id for update id_employee
     */
    private function updateDataCompany($data, $id)
    {
        EmployeesModel::where('id_employee', $id)
        ->update([
            'first_name' => ucwords($data->first_name),
            'last_name' => ucwords($data->last_name),
            'id_company' => $data->id_company,
            'email' => strtolower($data->email),
            'phone' => $data->phone,
            'password' => Hash::make($data->password),
            'updated_at' => Carbon::now(),
            'updated_by_id' => Session::get('jwt_data')->id,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        try {
            // delete data employee
            EmployeesModel::destroy($id);
            
            return redirect()->route('employees.index', app()->getLocale())->with('success', 'Employee Have Been Deleted');
        } catch (\Throwable $th) {

            return redirect()->route('employees.index', app()->getLocale())->with('error', 'Maintance');
        }
    }

    // Import Companies
    public function importEmployee(ValidateImportEmployee $request)
    {
        DB::beginTransaction();
        try {
             // Import Data Companies
            Excel::import(new EmployeeImport, $request->data_employee);
            // save data excel
            $this->saveImportDataEmployee($request);
            DB::commit();
            return redirect()->route('employees.index', app()->getLocale())->with('success', 'Import Data Employee Success');

        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->route('employees.index', app()->getLocale())->with('error', 'Maintance');
        }       
    }

    // function company for save import excel
    private function saveImportDataEmployee($data)
    {
        // save photo
        $file = $data->file('data_employee');
        $path = Storage::putFile('import-excel/',$file);
        return pathinfo($path)['basename'];
    }
}
