<?php

namespace App\Http\Controllers;

use App\Http\Requests\Items\ValidateItems;
use App\Models\ItemsModel;
use Carbon\Carbon;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = ItemsModel::all();

        return view('admin-panel/show.items-data', [
            'items_menu' => true,
            'index_menu' => true,
            'items' => $items
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-panel/input_data/create-items', ['items_menu' => true, 'create_menu' => true]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateItems $request)
    {
        try {

            ItemsModel::create([
                'name' => ucwords($request->name),
                'price' => $request->price,
                'created_at' => Carbon::now(),
            ]);

            return redirect()->route('items.index', app()->getLocale())->with('success', 'Items Created');
        } catch (\Throwable $th) {
            return redirect()->route('items.index', app()->getLocale())->with('error', 'Maintance');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $item = ItemsModel::find($id);

        return view('admin-panel/edit_data.edit-item', [
            'items_menu' => true,
            'index_menu' => true,
            'item' => $item,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateItems $request, $locale ,$id)
    {
        try {
            ItemsModel::where('id', $id)
                ->update([
                    'name' => ucwords($request->name),
                    'price' => $request->price,
                    'updated_at' => Carbon::now(),
                ]);
            
            return redirect()->route('items.index', app()->getLocale())->with('success', 'Items Updated');
        } catch (\Throwable $th) {
            return redirect()->route('items.index', app()->getLocale())->with('error', 'Maintance');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        try {
            ItemsModel::destroy($id);

            return redirect()->route('items.index', app()->getLocale())->with('success', 'Items Deleted');
        } catch (\Throwable $th) {
            return redirect()->route('items.index', app()->getLocale())->with('error', 'Maintance');
        }
    }
}
