<?php

namespace App\Http\Controllers;

use App\Http\Requests\Sells\ValidateSells;
use App\Models\EmployeesModel;
use App\Models\ItemsModel;
use App\Models\SellsModel;
use App\ServiceClass\SellsSummaryService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SellsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sells = SellsModel::with('item', 'employee')->get();

        return view('admin-panel/show.sells-data',[
            'sells_menu' => true,
            'index_sells' => true,
            'sells'=> $sells,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees  = EmployeesModel::all();
        $items      = ItemsModel::all();

        return view('admin-panel/input_data.create-sells',[
            'sells_menu' => true,
            'create_sells' => true,
            'employees' => $employees,
            'items' => $items,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateSells $request)
    {
        DB::beginTransaction();
        try {
            SellsModel::create([
                'created_date' => Carbon::now(),
                'item_id' => $request->item_id,
                'price' => $request->price,
                'discount' => ($request->discount / 100),
                'id_employee' => $request->id_employee,
            ]);
            
            // create sell Summary
            $sell_summary = new SellsSummaryService();
            $sell_summary->cratedSellSummary($request);

            DB::commit();
            return redirect()->route('sells.index', app()->getlocale())->with('success', 'Sells Created');
        } catch (\Throwable $th) {
            DB::rollback();
            return redirect()->route('sells.index', app()->getlocale())->with('error', 'Maintance');
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($locale, $id)
    {
        $sells = SellsModel::find($id);
        $employees  = EmployeesModel::all();
        $items      = ItemsModel::all();

        return view('admin-panel/edit_data.edit-sells',[
            'sells_menu' => true,
            'index_sells' => true,
            'employees' => $employees,
            'items' => $items,
            'sells' => $sells,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateSells $request, $locale, $id)
    {
        try {
            SellsModel::where('id', $id)
            ->update([
                'item_id' => $request->item_id,
                'price' => $request->price,
                'discount' => ($request->discount / 100),
                'id_employee' => $request->id_employee,
            ]);

            return redirect()->route('sells.index', app()->getlocale())->with('success', 'Sells Updated');
        } catch (\Throwable $th) {
            return redirect()->route('sells.index', app()->getlocale())->with('error', 'Maintance');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($locale, $id)
    {
        try {
            SellsModel::destroy($id);
            return redirect()->route('sells.index', app()->getlocale())->with('success', 'Sells Deleted');

        } catch (\Throwable $th) {
            return redirect()->route('sells.index', app()->getlocale())->with('error', 'Maintance');
        }
    }
}
