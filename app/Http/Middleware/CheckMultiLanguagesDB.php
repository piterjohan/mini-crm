<?php

namespace App\Http\Middleware;

use App\Models\LanguagesModel;
use Closure;

class CheckMultiLanguagesDB
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( !$request->session()->get('lang_data'))
        {
            // create lang
            $get_lang = LanguagesModel::find(1);
            $decode_lang = json_decode($get_lang->json_format);

            session(['lang_data' => $decode_lang ]);
            return $next($request);
        }
        return $next($request);
    }
}
