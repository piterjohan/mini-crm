<?php

namespace App\Http\Requests\Companies;

use Illuminate\Foundation\Http\FormRequest;
use App;

class ValidateCreateCompanies extends FormRequest
{
    public function getLanguage()
    {
        $locale = app()->getLocale();
        return $locale;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'logo' => 'dimensions:min_width=100,min_height=100|mimes:jpg,jpeg,png',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        if($this->getLanguage() == "en")
        {
            return [
                'name.required' => 'Company Name field is required',
                'email.required' => 'Email Company field is required',
                'logo.dimensions' => 'Minimal Logo Company 100 x 100',
                'logo.mimes' => 'Invalid type photo. Allowed: JPG, JPEG, PNG',
            ];
        }else{
            return [
                'name.required' => 'Harap Isi Nama Perusahaan',
                'email.required' => 'Harap Isi Email Perusahaan',
                'logo.dimensions' => 'Minimal Logo Company 100 x 100',
                'logo.mimes' => 'Tipe tidak diijinkan. Diperbolehkan: JPG, JPEG, PNG',
            ];
        }
    }

}
