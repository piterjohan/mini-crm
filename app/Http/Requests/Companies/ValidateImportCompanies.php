<?php

namespace App\Http\Requests\Companies;

use Illuminate\Foundation\Http\FormRequest;

class ValidateImportCompanies extends FormRequest
{
    public function getLanguage()
    {
        $locale = app()->getLocale();
        return $locale;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data_company' => 'required|mimes:xlsx,xls',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        if($this->getLanguage() == "en")
        {
            return [
                'data_company.required' => 'Import Data Company field is required',
                'data_company.mimes' => 'Invalid type data. Allowed: xlsx, xls',
            ];
        }else{
            return [
                'data_company.required' => 'Harap Upload Import Data Perusahaan',
                'logo_company.mimes' => 'Tipe tidak diijinkan. Diperbolehkan: xlsx, xls',
            ];
        }
    }
}

