<?php

namespace App\Http\Requests\Employees;

use Illuminate\Foundation\Http\FormRequest;

class ValidateCreateEmployees extends FormRequest
{
    public function getLanguage()
    {
        $locale = app()->getLocale();
        return $locale;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_company' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'required|min:12',
            'password' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        if($this->getLanguage() == "en")
        {
            return [
                'id_company.required' => 'Company field is required',
                'first_name.required' => 'First Name field is required',
                'logo_company.required' => 'Last Name field is required',
                'email.required' => 'Employee Email field is required',
                'phone.required' => 'Employee Phone field is required',
                'phone.min' => 'Employee Phone minimun 12 digit',
                'password.required' => 'Password field is required',
            ];
        }else{
            return [
                'id_company.required' => 'Harap Pilih Perusahaan',
                'first_name.required' => 'Harap Isi Nama Depan',
                'logo_company.required' => 'Harap Isi Nama Belakang',
                'email.required' => 'Harap Isi Email Karyawan',
                'phone.required' => 'Harap Isi No Handphone Karyawan',
                'phone.min' => 'No Handphone Karyawan minimal 12 digit',
                'password.required' => 'Harap Isi Kata Sandi',
            ];
        }
    }
}
