<?php

namespace App\Http\Requests\Employees;

use Illuminate\Foundation\Http\FormRequest;

class ValidateImportEmployee extends FormRequest
{
    public function getLanguage()
    {
        $locale = app()->getLocale();
        return $locale;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data_employee' => 'required|mimes:xlsx,xls',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        if($this->getLanguage() == "en")
        {
            return [
                'data_employee.required' => 'Import Data Employee field is required',
                'data_employee.mimes' => 'Invalid type data. Allowed: xlsx, xls',
            ];
        }else{
            return [
                'data_employee.required' => 'Harap Upload Import Data Karyawan',
                'data_employee.mimes' => 'Tipe tidak diijinkan. Diperbolehkan: xlsx, xls',
            ];
        }
    }
}
