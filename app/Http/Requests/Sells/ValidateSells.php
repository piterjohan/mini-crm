<?php

namespace App\Http\Requests\Sells;

use Illuminate\Foundation\Http\FormRequest;

class ValidateSells extends FormRequest
{
    public function getLanguage()
    {
        $locale = app()->getLocale();
        return $locale;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_id' => 'required',
            'price' => 'required',
            'id_employee' => 'required',
            'discount' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        if ($this->getLanguage() == "en") 
        {
            return [
                'item_id.required' => 'Item Name is required',
                'price.required' => 'Price is required',
                'id_employee.required' => 'Employee is required',
                'discount.required' => 'Discount is required',
            ];
        }else{
            return [
                'item_id.required' => 'Harap pilih item',
                'price.required' => 'Harap isi harga',
                'id_employee.required' => 'Harap pilih karyawan',
                'discount.required' => 'Harap isi diskon',
            ];
        }
    }
}
