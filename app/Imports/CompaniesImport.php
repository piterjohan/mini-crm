<?php

namespace App\Imports;

use App\Models\CompaniesModel;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon\Carbon;

class CompaniesImport implements ToModel, WithHeadingRow
{
    
    public function model(array $row)
    {        
        return new CompaniesModel([
            'name'  => $row['company_name'],
            'email' => $row['email_company'],
            'created_at'    => Carbon::now(),
        ]);
    }
}
