<?php

namespace App\Imports;

use App\Models\EmployeesModel;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Carbon\Carbon;

class EmployeeImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {        
        return new EmployeesModel([
            'first_name' => $row['first_name'], 
            'last_name' => $row['last_name'], 
            'id_company' => $row['company_id'], 
            'email' => $row['employee_email'], 
            'phone' => $row['employee_phone'],
            'created_at' => Carbon::now(),
        ]);
    }
}
