<?php

namespace App\Jobs;

use App\Models\CompaniesModel;
use App\Mail\NotificationCreateCompany;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailCompanies implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    public $email;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email)
    {   
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dataCompany = CompaniesModel::where('email', $this->email)->first();

        $emailCompany = new NotificationCreateCompany($dataCompany);
        Mail::to($this->email)->send($emailCompany);

    }
}
