<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompaniesModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id_company';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'logo', 'created_at', 'updated_at', 'created_by_id', 'updated_by_id',];
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

     /**
     * Get the employees for the companies.
     */
    public function employee ()
    {
        return $this->hasMany('App\Models\EmployeesModel', 'id_company');
    }
}
