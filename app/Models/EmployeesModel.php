<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeesModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id_employee';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'id_company',
        'email',
        'phone',
        'password',
        'created_at',
        'updated_at',
        'created_by_id',
        'updated_by_id',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get the company for the employee.
     */
    public function company()
    {
        return $this->belongsTo('App\Models\CompaniesModel', 'id_company');
    }

    /**
     * Get info sell by employees id
     */

     public function sell()
     {
         return $this->hasMany('App\Models\SellsModel', 'id_employee');
     }

     /**
      * Get info Sumarry by employees id
      */
    public function sellSummary()
    {
        return $this->hasMany('App\Models\SellsSummaryModel', 'id_employee');
    }
}
