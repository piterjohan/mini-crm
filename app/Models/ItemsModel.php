<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemsModel extends Model
{
    protected $table = 'items';

    protected $primaryKey  = 'id';

    protected $fillable = [
        'id',
        'name',
        'price',
        'created_at',
        'updated_at'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Get sells from foreign
     */
    public function sell()
    {
        return $this->hasMany('App\Models\SellsModel', 'item_id');
    }
}
