<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SellsModel extends Model
{
    protected $table = 'sells';

    protected $primaryKey  = 'id';

    protected $fillable = [
        'id',
        'created_date',
        'item_id',
        'price',
        'discount',
        'id_employee'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * Get item from foreign
     */
    public function item()
    {
        return $this->belongsTo('App\Models\ItemsModel', 'item_id');
    }

    /**
     * Get employee by id employee
     */
    public function employee()
    {
        return $this->belongsTo('App\Models\EmployeesModel', 'id_employee');
    }

    /**
     * Get summary by id_employee
     */
    public function sellSummary()
    {
        return $this->belongsTo('App\Models\SellsSummaryModel', 'id_employee', 'id_employee');
    }

}
