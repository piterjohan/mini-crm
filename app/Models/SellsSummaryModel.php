<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SellsSummaryModel extends Model
{
    protected $table = 'sells_summary';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'id_employee',
        'date',
        'created_date',
        'last_update',
        'price_total',
        'discount_total',
        'total'
    ];

     /**
     * Get employee by id employee
     */
    public function employee()
    {
        return $this->belongsTo('App\Models\EmployeesModel', 'id_employee');
    }

    /**
     * Get sells by id_employee
     */
    public function sell()
    {
        return $this->hasMany('App\Models\SellsModel', 'id_employee', 'id_employee');
    }
    
}
