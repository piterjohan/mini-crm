<?php

namespace App\ServiceClass;

use App\Http\Requests\Sells\ValidateSells;
use App\Models\CompaniesModel;
use App\Models\SellsModel;
use App\Models\SellsSummaryModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SellsSummaryService
{

    /**
     * create sell summary
     * @storeRequest ValidateSells
     * @param request $request
     */
    public function cratedSellSummary(ValidateSells $request)
    {
        $have_sell_summary = $this->checkSellsSummary(Carbon::today()->toDateString(), $request->id_employee);

        ($have_sell_summary) ? $this->accumulationSells($request->id_employee) : $this->saveSellSummary($request);
    }

    /**
     * Acummulation sell summary
     * @param String $id_employee
     */
    protected function accumulationSells($id_employee)
    {
        $date_now = Carbon::today()->toDateString();
        $accumulation_sell = SellsModel::whereDate('created_date', $date_now)
            ->where('id_employee', $id_employee)
            ->select(
                DB::raw('SUM(price) as price'),
                DB::raw('SUM(price * discount) as discount')
            )->first();

        $this->updateExistingSellSummary($accumulation_sell, $date_now, $id_employee);
    }

    /**
     * update existing sells summary
     * @param collection $data_sell
     * @param String $id_employee
     * @param String $id_employee
     */
    protected function updateExistingSellSummary($accumulation_sell, $date_now, $id_employee)
    {
        $date_now = Carbon::today()->toDateString();

        $existing_summary = SellsSummaryModel::Where('date', date($date_now))
            ->where('id_employee', $id_employee)->first();

        $total = ($accumulation_sell->price - $accumulation_sell->discount);
        
        // update Existing Sell Summary
        SellsSummaryModel::where('id', $existing_summary->id)
            ->update([
                'last_update' => Carbon::now(),
                'price_total' => $accumulation_sell->price,
                'discount_total' => $accumulation_sell->discount,
                'total' => $total,
            ]);
    }

    /**
     * save sell summary
     * @param request $request
     * 
     */
    protected function saveSellSummary($request)
    {
        SellsSummaryModel::create([
            'id_employee' => $request->id_employee,
            'date' => Carbon::today()->toDateString(),
            'created_date' => Carbon::now(),
            'last_update' => Carbon::now(),
            'price_total' => $request->price,
            'discount_total' => ( ($request->price * ($request->discount / 100)) ),
            'total' =>  $request->price,
        ]);
    }

    /**
     * Check existing sell summary
     * @param date $date
     * @param String id_employee
     */
    protected function checkSellsSummary(String $date, $id_employee)
    {
        return SellsSummaryModel::where('date', date($date))
            ->where('id_employee', $id_employee)->count();
    }

    /**
     * searching sells summary by filter 
     * on menu sell summary
     * @param Request $request 
     * @return array key & collection
     * key to manage data view
     */
    public function filterSearchingSection($request)
    {
        // search date
        if ($request->start_date && $request->end_date)
        {
            return $this->sellSummaryByDate($request);
        }

        if ($request->employee)
        {
            return $this->sellSummaryEmployees($request);
        }

        if ($request->company)
        {
            return $this->sellSummaryCompanies($request);
        }

    }



    /**
     * Searching sell summary by date
     */
    protected function sellSummaryByDate($request)
    {
        $sells_summary = SellsSummaryModel::with('employee')
            ->where('date', '>=', $request->start_date)
            ->where('date', '<=', $request->end_date)
            ->get();
        
        return ['date', $sells_summary];
    }

    /**
     * get sell summary base on employee
     * @param Request $request
     * @return Array [ employee, Collection $sell_summary ]
     * key employee to manage data
     */
    protected function sellSummaryEmployees($request)
    {
        $sells_summary = SellsSummaryModel::with('employee')
            ->where('id_employee', $request->employee)
            ->get();

        return ['employee', $sells_summary];
    }

    /**
     * get sell summary base on company
     * @param Request $request
     * @return Array [ employee, Collection $company ]
     * key company to manage data
     */
    protected function sellSummaryCompanies($request)
    {
        $company = CompaniesModel::find($request->company);

        foreach ($company->employee  as $employee) {
            $sells_summary = SellsSummaryModel::where('id_employee', $employee->id_employee)
                            ->get();
            $employee->sellsSummary = $sells_summary;
        }
        return ['company', $company];
    }

    /**
     * Get sells data from id sells_summary
     * @param String $id
     */
    public function getSells($id)
    {
        $sell_summary = SellsSummaryModel::find($id);
        $get_sell = SellsModel::with('employee', 'item')
                    ->where('id_employee', $sell_summary->id_employee)
                    ->whereDate('created_date' , $sell_summary->date)
                    ->get();

        return $get_sell;
    }
}
