<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CompaniesModel;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(CompaniesModel::class, function (Faker $faker) {
    return [
        'name' => $faker->company, 
        'email' => $faker->companyEmail,
        'logo' => $faker->imageUrl,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
        'created_by_id' => null,
        'updated_by_id' => null,
    ];
});
