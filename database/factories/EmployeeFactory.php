<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\EmployeesModel;
use Faker\Generator as Faker;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

$factory->define(EmployeesModel::class, function (Faker $faker) {

    return [
        'first_name' => $faker->name,
        'last_name' => $faker->lastName,
        'id_company' => $faker->numberBetween(1, 10),
        'email' => $faker->email,
        'phone' => $faker->e164PhoneNumber,
        'password' => Hash::make("123"),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
        'created_by_id' => null,
        'updated_by_id' => null,
    ];
});
