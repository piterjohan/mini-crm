<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Models\ItemsModel;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(ItemsModel::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price' => $faker->numberBetween($min = 500000, $max = 500000000),
        'created_at' => Carbon::now()
    ];
});
