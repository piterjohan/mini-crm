<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SellsModel;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(SellsModel::class, function (Faker $faker) {
    return [
        'created_date' => Carbon::now(),
        'item_id' => $faker->numberBetween(1, 10),
        'price' => $faker->numberBetween(500000, 500000000),
        'discount' => rand(0.10, 1),
        'id_employee' => $faker->numberBetween(1, 10),
    ];
});
