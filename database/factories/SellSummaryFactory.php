<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\SellsSummaryModel;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(SellsSummaryModel::class, function (Faker $faker) {
    return [
        'id_employee' => $faker->numberBetween(1, 10),
        'date' => Carbon::today()->toDateString(),
        'created_date' => Carbon::now(),
        'last_update' => Carbon::now(),
        'price_total' => $faker->numberBetween(500000, 500000000),
        'discount_total' => $faker->numberBetween(0.5, 0.80),
        'total' => $faker->numberBetween(500000, 500000000),
    ];
});
