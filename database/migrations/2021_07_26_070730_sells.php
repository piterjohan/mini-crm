<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Sells extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sells', function (Blueprint $table) {
            $table->id();
            $table->dateTime('created_date');
            $table->unsignedBigInteger('item_id');
            $table->integer('price');
            $table->decimal('discount', 4, 2);
            $table->unsignedBigInteger('id_employee');

            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('id_employee')->references('id_employee')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sells');
    }
}
