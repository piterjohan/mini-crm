<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SellsSummary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sells_summary', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_employee');
            $table->date('date');
            $table->dateTime('created_date');
            $table->dateTime('last_update');
            $table->integer('price_total');
            $table->decimal('discount_total', 12, 2);
            $table->decimal('total', 12, 2);

            $table->foreign('id_employee')->references('id_employee')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sells_summary');
    }
}
