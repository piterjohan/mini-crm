<?php

use App\Models\LanguagesModel;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LanguagesModel::create([
            'json_format' => json_encode([
                [
                    "lang" => "en",
                    'icon' => "flag-icon-usa"
                ],
                [
                    "lang" => "id",
                    'icon' => "flag-icon-idn"
                ],
            ]),
            'created_at' => Carbon::now(),
        ]);
    }
}
