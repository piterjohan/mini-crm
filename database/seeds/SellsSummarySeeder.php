<?php

use App\Models\CompaniesModel;
use App\Models\EmployeesModel;
use App\Models\ItemsModel;
use App\Models\SellsModel;
use App\Models\SellsSummaryModel;
use Illuminate\Database\Seeder;

class SellsSummarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // company
        $company = factory(CompaniesModel::class, 10)->create()->each(function ($company) {
            // employee
            $employee = factory(EmployeesModel::class)->make();
            $company->employee()->save($employee);
        });
        
        //item
        $items = factory(ItemsModel::class, 10)->create();

        // sell
        factory(SellsModel::class)->create(['created_date' => '2021-07-26', 'id_employee' => 2]);
        factory(SellsModel::class)->create(['created_date' => '2021-07-26', 'id_employee' => 2]);
        factory(SellsModel::class)->create(['created_date' => '2021-07-27', 'id_employee' => 3]);
        factory(SellsModel::class)->create(['created_date' => '2021-07-27', 'id_employee' => 3]);
        factory(SellsModel::class)->create(['created_date' => '2021-07-28', 'id_employee' => 4]);
        factory(SellsModel::class)->create(['created_date' => '2021-07-28', 'id_employee' => 4]);

        // sell Summary
        factory(SellsSummaryModel::class)->create(['date' => '2021-07-26', 'id_employee' => 2]);
        factory(SellsSummaryModel::class)->create(['date' => '2021-07-27', 'id_employee' => 3]);
        factory(SellsSummaryModel::class)->create(['date' => '2021-07-28', 'id_employee' => 4]);
       
    }
}
