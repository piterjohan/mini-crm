<?php

return [

    /*
    |Menu All Data
    |
    */

    'titleAllData' => 'All Data',
    'titleCompaniesCard' => 'Data Companies',
    'titleEmployeesCard' => 'Data Employees',
    'titleCurentTimezone' => 'Timezone',
    // placeholder
    'labelplaceholderSearchTimezone' => 'Search your timezone',
    'labelPlaceHolderSearching' => 'Searching Company',
    'labelPlaceHolderSearchingByDate' => 'Please choose a date',

    // label
    'labelCreatedAt' => 'Created:',
    'labelUpdatedAt' => 'Updated:',

    // bio data Employee
    'ParagrafPhone' => 'Phone',
    'ParagrafCompanyName' => 'Company Name',
    

];
