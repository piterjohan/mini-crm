<?php

return [

    /*
    | Menu Companies > Company Data
    |
    */

    'titleCompanyData' => 'Data Company',
    /**
     * table
     */
    'tableNumber' => 'NB',
    'tableidCompany' => 'Company ID',
    'tableCompanyName' => 'Company Name',
    'tableCompanyEmail' => 'Company Email',
    'tableCompanyLogo' => 'Company Logo',
    'tableAction'=> 'Action',

    /*
    | Menu Companies > Create Company
    |
    */
    'titleCreateCompany' => 'Create Company',
    'titleUpdateCompany' => 'Update Company',
    // form
    'LabelCompanyName' => 'Company Name',
    'LabelEmailCompany' => 'Email Company',
    'LabelLogoCompany' => 'Logo Company',
    // import
    'LabelImportCompany' => 'Import Data Company',
    'LabelDownloadFormatImportCompany' => 'Download Format Excel Company',

    
    
];
