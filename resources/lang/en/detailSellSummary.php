<?php

return [
    /**
     * Menu > Sells > Sell Summary Detail
     */
    'titleDetailSellSummary' => 'Details Sell Summary',

    // table title
    'tableDetailTitleSellSummary' => 'List Detail Sell Summary',
    
    //title
    'tableNumber' => 'NB',
    'tableCreatedDate' => 'Created Date',
    'tableItemName' => 'Item Name',
    'tablePrice' => 'Price',
    'tableDiscount' => 'Discount',
    'tableEmployee' => 'Employee Name',

];