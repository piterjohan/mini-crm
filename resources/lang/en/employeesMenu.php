<?php

return [

    /*
    | Menu Employees > Employee Data
    |
    */

    'titleEmployeeData' => 'Data Employee',
    /**
     * table
     */
    'tableNumber' => 'NB',
    'tableFirstName' => 'First Name',
    'tableLastName' => 'Last Name',
    'tableEmail' => 'Email',
    'tablePhone' => 'Phone',
    'tableCompanyName' => 'Company Name',
    'tableAction'=> 'Action',


    /*
    | Menu Employees > Create Employee
    |
    */
    'titleCreateEmployee' => 'Create Employee',
    'titleUpdateEmployee' => 'Update Employee',
    // form
    'LabelCompany' => 'Company',
    'LabelSelectCompany' => 'Choose Company',
    'LabelEmailCompany' => 'Email Company',
    'LabelFirstName' => 'First Name',
    'LabelLastName' => 'Last Name',
    'LabelEmail' => 'Employee Email',
    'LabelPhone' => 'Employee Phone',
    'LabelPassword' => 'Employee Password',
    // import
    'LabelImportEmployee' => 'Import Data Employee',
    'LabelDownloadFormatImportEmployee' => 'Download Format Excel Employee',
    // PlaceHolder
    'LabelPlaceHolderSearching' => 'Searching Employee',
    'LabelPlaceHolderSearchingByDate' => 'Please choose a date',

];
