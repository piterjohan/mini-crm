<?php

return [

    /*
    | Menu Items > Create Items
    |
    */

    'titleItemsMenu' => 'Items',
    // Label
    'LabelItemName' => 'Item Name',
    'LabelPrice' => 'Price',

    // placeholder
    'placeholderINDCurrency' => 'EX: 25000',

    // table
    'tableNumber' => 'NB',
    'tableItemName' => 'Name',
    'tableItemPrice' => 'Price',
    'tableAction' => 'Action',

    /*
    | Menu Items > Edit Items
    |
    */
    'titleUpdateItemsMenu' => 'Form Update Items',
];