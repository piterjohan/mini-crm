<?php

return [

    /*
    | Label Action for name like edit,delete,submit,etc
    |
    */
    'backButtonAction' => 'Back',
    'detailsButtonAction' => 'Details',
    'editButtonAction' => 'Edit',
    'deleteButtonAction' => 'Delete',
    'saveButtonAction' => 'Submit',
    'closeButtonAction' => 'Close',
    'warningDeleteAction' => 'Are You Sure Delete This Data?',
    'searchButtonAction' => 'Search',
];
