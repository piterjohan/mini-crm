<?php

return [
    /**
     * Menu > Sells > Sell Summary
     */
    'titleSellSummary' => 'Sell Summary',

    // table title
    'tableTitleSellSummary' => 'List Sell Summary',

    // placeholder
    'placeholderStartDate' => 'Date start',
    'placeholderEndDate' => 'Date end',

    // label
    'labelEmployee' => 'Choose Employee',
    'labelCompany' => 'Choose Company',
    
    //title
    'tableNumber' => 'NB',
    'tableEmployeeName' => 'Employee Name',
    'tableDate' => 'Date',
    'tablePriceTotal' => 'Total Price',
    'tableDiscountTotal' => 'Total Discount',
    'tableTotal' => 'Total',
    'tableAction' => 'Action',

];