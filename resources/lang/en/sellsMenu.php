<?php

return [

    /*
    | Menu Items > Create Sells
    |
    */

    'titleSellsMenu' => 'Sells',
    // Label
    'LabelItem' => 'Items',
    'LabelItemPrice' => 'Price',
    'LabelEmployeeName' => 'Employee',
    'LabelItemDiscount' => 'Discount',

    // placeholder
    'placeholderCurrency' => 'EX: 25000',
    'placeholderDiscount' => 'EX: 25',
    'placeholderSelectEmployee' => 'Choose Employee',
    'placeholderSelectItem' => 'Choose Item',

    // table
    'tableNumber' => 'NB',
    'tableCreatedData' => 'Date',
    'tableItemName' => 'Item Name',
    'tableItemPrice' => 'Price',
    'tableDiscount' => 'Discount',
    'tableEmployee' => 'Employee',
    'tableAction' => 'Action',

    /*
    | Menu Items > Edit Sells
    |
    */
    'titleUpdateSellsMenu' => 'Form Update Sell',
];