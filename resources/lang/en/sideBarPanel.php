<?php

return [

    /*
    | Sidebar Panel
    |
    */
    'headerName' => 'Name',
    'headerEmail' => 'Email',
    'headerLogOut' => 'LogOut',
    'titleHeaderNavigation' => 'MAIN NAVIGATION',
    // top menu
    'titleFilterData' => 'Filter Data',
    // first Menu
    'titleFirstMenu' => 'Companies',
    'subOneTitleFirstMenu' => 'Company Data',
    'subTwoTitleFirstMenu' => 'Create Company',
    // second Menu
    'titleSecondMenu' => 'Employees',
    'subOneTitleSecondMenu' => 'Employe Data',
    'subTwoTitleSecondMenu' => 'Create Employee',
    // third Menu
    'titleThirdMenu' => 'Items',
    'subOneTitleThirdMenu' => 'Items Data',
    'subTwoTitleThirdMenu' => 'Create Items',
    // fourth menu
    'titleFourthMenu' => 'Sells',
    'subOneTitleFourthMenu' => 'Sells Data',
    'subTwoTitleFourthMenu' => 'Create Sell',
    'subThreeTitleFourthMenu' => 'Sell Summary',
    // Footer
    'version' => 'Version',
];
