<?php

return [

    /*
    |Menu All Data
    |
    */

    'titleAllData' => 'Semua Data',
    'titleCompaniesCard' => 'Data Perusahaan',
    'titleEmployeesCard' => 'Data Karyawan',
    'titleCurentTimezone' => 'Zona Waktu',
    // placeholder
    'labelplaceholderSearchTimezone' => 'Cari timezone anda',
    'labelPlaceHolderSearching' => 'Pencaharian Perusahaan',
    'labelPlaceHolderSearchingByDate' => 'Pilih Tanggal',

    // label
    // label
    'labelCreatedAt' => 'Dibuat:',
    'labelUpdatedAt' => 'Diupdated:',

    // bio data Employee
    'ParagrafPhone' => 'No Handphone',
    'ParagrafCompanyName' => 'Nama Perusahaan',
    

];
