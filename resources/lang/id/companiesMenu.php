<?php

return [

    /*
    | Menu Companies > Company Data
    |
    */

    'titleCompanyData' => 'Data Perusahaan',
    /**
     * table
     */
    'tableNumber' => 'No',
    'tableidCompany' => 'ID Perusahaan',
    'tableCompanyName' => 'Nama Perusahaan',
    'tableCompanyEmail' => 'Email Perusahaan',
    'tableCompanyLogo' => 'Logo Perusahaan',
    'tableAction'=> 'Aksi',

    /*
    | Menu Companies > Create Company
    |
    */
    'titleCreateCompany' => 'Buat Perusahaan',
    'titleUpdateCompany' => 'Ubah Perusahaan',
    // form
    'LabelCompanyName' => 'Nama Perusahaan',
    'LabelEmailCompany' => 'Email Perusahaan',
    'LabelLogoCompany' => 'Logo Perusahaan',
    // import
    'LabelImportEmployee' => 'Import Data Perusahaan',
    'LabelDownloadFormatImportEmployee' => 'Mengunduh Format Excel Perusahaan',
    


];
