<?php

return [
    /**
     * Menu > Sells > Sell Summary Detail
     */
    'titleDetailSellSummary' => 'Detail Ringkasan Penjualan',

    // table title
    'tableDetailTitleSellSummary' => 'List Ringkasan Penjualan',
    
    //title
    'tableNumber' => 'No',
    'tableCreatedDate' => 'Tanggal Pembuatan',
    'tableItemName' => 'Nama Item',
    'tablePrice' => 'Harga',
    'tableDiscount' => 'Diskon',
    'tableEmployee' => 'Nama Karyawan',

];