<?php

return [

    /*
    | Menu Employees > Employee Data
    |
    */

    'titleEmployeeData' => 'Data Karyawan',
    /**
     * table
     */
    'tableNumber' => 'NB',
    'tableFirstName' => 'Name Depan',
    'tableLastName' => 'Nama Belakang',
    'tableEmail' => 'Email',
    'tablePhone' => 'No Handphone',
    'tableCompanyName' => 'Nama Perusahaan',
    'tableAction'=> 'Aksi',


    /*
    | Menu Employees > Create Employee
    |
    */
    'titleCreateEmployee' => 'Buat Data Karyawan',
    'titleUpdateEmployee' => 'Ubat Data Karyawan',
    // form
    'LabelCompany' => 'Perusahaan',
    'LabelSelectCompany' => 'Pilih Perusahaan',
    'LabelEmailCompany' => 'Email Perusahaan',
    'LabelFirstName' => 'Nama Depan',
    'LabelLastName' => 'Nama Belakang',
    'LabelEmail' => 'Email Karyawan',
    'LabelPhone' => 'No Handphone Karyawan',
    'LabelPassword' => 'Kata Sandi Karyawan',
    // import
    'LabelImportEmployee' => 'Import Data Karyawan',
    'LabelDownloadFormatImportEmployee' => 'Mengunduh Format Excel Karyawan',
    // PlaceHolder
    'LabelPlaceHolderSearching' => 'Pencaharian Karyawan',
    'LabelPlaceHolderSearchingByDate' => 'Pilih Tanggal',

];
