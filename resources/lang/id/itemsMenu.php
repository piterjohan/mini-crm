<?php

return [

    /*
    | Menu Items > Create Items
    |
    */

    'titleItemsMenu' => 'Item',
    // Label
    'LabelItemName' => 'Nama Item',
    'LabelPrice' => 'Harga',

    // placeholder
    'placeholderINDCurrency' => 'Contoh: 25000',

    // table
    'tableNumber' => 'No',
    'tableItemName' => 'Nama',
    'tableItemPrice' => 'Harga',
    'tableAction' => 'Aksi',

    /*
    | Menu Items > Edit Items
    |
    */
    'titleUpdateItemsMenu' => 'Form Ubah Item',
];