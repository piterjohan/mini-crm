<?php

return [

    /*
    | Label Action for name like edit,delete,submit
    |
    */
    'backButtonAction' => 'Kembali',
    'detailsButtonAction' => 'Selengkapnya',
    'editButtonAction' => 'Ubah',
    'deleteButtonAction' => 'Hapus',
    'saveButtonAction' => 'Simpan',
    'closeButtonAction' => 'Tutup',
    'warningDeleteAction' => 'Yakin Menghapus Data Ini?',
    'searchButtonAction' => 'Cari',

];
