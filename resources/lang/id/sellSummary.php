<?php

return [
    /**
     * Menu > Sells > Sell Summary
     */
    'titleSellSummary' => 'Ringkasan Penjualan',

    // table title
    'tableTitleSellSummary' => 'List Ringkasan Penjualan',
    
    // placeholder
    'placeholderStartDate' => 'Tanggal Awal',
    'placeholderEndDate' => 'Tanggal Akhir',

    // label
    'labelEmployee' => 'Pilih Karyawan',
    'labelCompany' => 'Pilih Perusahaan',

    //title
    'tableNumber' => 'No',
    'tableEmployeeName' => 'Nama Karyawan',
    'tableDate' => 'Tanggal',
    'tablePriceTotal' => 'Total Harga',
    'tableDiscountTotal' => 'Total Diskon',
    'tableTotal' => 'Total',
    'tableAction' => 'Aksi',

];