<?php

return [

    /*
    | Menu Items > Create Sells
    |
    */

    'titleSellsMenu' => 'Penjualan',
    // Label
    'LabelItem' => 'Item',
    'LabelItemPrice' => 'Harga',
    'LabelEmployeeName' => 'Karyawan',
    'LabelItemDiscount' => 'Diskon',

    // placeholder
    'placeholderCurrency' => 'EX: 25000',
    'placeholderDiscount' => 'EX: 25',
    'placeholderSelectEmployee' => 'Pilih Karyawan',
    'placeholderSelectItem' => 'Pilih Item',

    // table
    'tableNumber' => 'No',
    'tableCreatedData' => 'Tanggal',
    'tableItemName' => 'Nama Item',
    'tableItemPrice' => 'Harga',
    'tableDiscount' => 'Diskon',
    'tableEmployee' => 'Karyawan',
    'tableAction' => 'Aksi',
    /*
    | Menu Items > Edit Sells
    |
    */
    'titleUpdateSellsMenu' => 'Form Ubah Penjualan',
];