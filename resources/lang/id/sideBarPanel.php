<?php

return [

    /*
    | Sidebar Panel
    |
    */
    'headerName' => 'Nama',
    'headerEmail' => 'Email',
    'headerLogOut' => 'Keluar',
    'titleHeaderNavigation' => 'Navigasi Utama',
    // top menu
    'titleFilterData' => 'Filter Data',
    // first Menu
    'titleFirstMenu' => 'Perusahaan',
    'subOneTitleFirstMenu' => 'Data Perusahaan',
    'subTwoTitleFirstMenu' => 'Buat Perusahaan',
    // second Menu
    'titleSecondMenu' => 'Karyawan',
    'subOneTitleSecondMenu' => 'Data Karyawan',
    'subTwoTitleSecondMenu' => 'Buat Data Karyawan',
    // third Menu
    'titleThirdMenu' => 'Item',
    'subOneTitleThirdMenu' => 'Data Item',
    'subTwoTitleThirdMenu' => 'Buat Item',
    // fourth menu
    'titleFourthMenu' => 'Penjualan',
    'subOneTitleFourthMenu' => 'Data Penjualan',
    'subTwoTitleFourthMenu' => 'Buat Penjualan',
    'subThreeTitleFourthMenu' => 'Ringkasan Penjualan',
    // Footer
    'version' => 'Versi',
];
