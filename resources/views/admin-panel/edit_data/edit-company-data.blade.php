@extends('layouts.adminPanel')

@section('title', __('companiesMenu.titleUpdateCompany') )

@section('custom-css')
    <!-- Bootstrap Core Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset ('adminSB/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset ('adminSB/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="{{ asset ('adminSB/plugins/multi-select/css/multi-select.css')}}" rel="stylesheet">
   
    <!-- Bootstrap Select Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset ('adminSB/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset ('adminSB/css/themes/all-themes.css')}}" rel="stylesheet" />
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>{{ __('companiesMenu.titleUpdateCompany') }}</h2>
            </div>

            <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Form {{ __('companiesMenu.titleUpdateCompany') }}
                            </h2>
                        </div>
                        <div class="body">
                            {{-- alert --}}
                            @include('partials.alert')

                            <form action="{{ route('companies.update', [ app()->getLocale(), $company->id_company ] ) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                                <label for="name">{{ __('companiesMenu.LabelCompanyName') }}</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="name" value="{{ $company->name }}" id="company_name" class="form-control" placeholder="{{ __('companiesMenu.LabelCompanyName') }}" required>
                                    </div>
                                </div>

                                <label for="email">{{ __('companiesMenu.LabelEmailCompany') }}</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="email" name="email" value="{{ $company->email }}" id="email" class="form-control" placeholder="{{ __('companiesMenu.LabelEmailCompany') }}" required>
                                    </div>
                                </div>

                                <label for="logo">{{ __('companiesMenu.LabelLogoCompany') }}</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="file" name="logo" id="logo" class="form-control">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">{{ __('labelAction.saveButtonAction') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Vertical Layout -->

        </div>
    </div>

        
</section>
    
@endsection

@section('custom-js')
    <!-- Jquery Core Js -->
    <script src="{{ asset ('adminSB/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/node-waves/waves.js')}}"></script>

     <!-- Jquery DataTable Plugin Js -->
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{ asset ('adminSB/js/admin.js')}}"></script>
    <script src="{{ asset ('adminSB/js/pages/tables/jquery-datatable.js')}}"></script>
    <script src="{{ asset ('adminSB/js/pages/forms/advanced-form-elements.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{ asset ('adminSB/js/demo.js')}}"></script>
@endsection