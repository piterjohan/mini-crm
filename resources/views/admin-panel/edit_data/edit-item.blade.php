@extends('layouts.adminPanel')

@section('title', __('itemsMenu.titleItemsMenu') )

@section('custom-css')
    <!-- Bootstrap Core Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset ('adminSB/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset ('adminSB/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="{{ asset ('adminSB/plugins/multi-select/css/multi-select.css')}}" rel="stylesheet">
   
    <!-- Bootstrap Select Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset ('adminSB/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset ('adminSB/css/themes/all-themes.css')}}" rel="stylesheet" />
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>{{ __('itemsMenu.titleUpdateItemsMenu') }}</h2>
            </div>

            <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ __('itemsMenu.titleUpdateItemsMenu') }}
                            </h2>
                        </div>
                        @include('partials.modals')
                        <div class="body">
                            {{-- alert --}}
                            @include('partials.alert')

                            <form action="{{ route('items.update', [app()->getLocale(), $item->id] ) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                
                                <label for="name">{{ __('itemsMenu.LabelItemName') }}</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="name" value="{{ $item->name }}" id="name" class="form-control" placeholder="{{ __('itemsMenu.LabelItemName') }}" required>
                                    </div>
                                </div>

                                <label for="price">{{ __('itemsMenu.LabelPrice') }}</label>
                                <div class="input-group">
                                    <div class="form-line demo-masked-input">
                                        <input type="text" class="form-control" value="{{ $item->price }}" name="price" pattern="[\d]+" title="Please Insert Number Only" placeholder="{{ __('itemsMenu.placeholderINDCurrency') }}">
                                    </div>
                                </div>
                              
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">{{ __('labelAction.saveButtonAction') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Vertical Layout -->

        </div>
    </div>

        
</section>
    
@endsection

@section('custom-js')
    <!-- Jquery Core Js -->
    <script src="{{ asset ('adminSB/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
    
    <!-- Input Mask Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/node-waves/waves.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{ asset ('adminSB/js/admin.js')}}"></script>
    {{-- <script src="{{ asset ('adminSB/js/pages/forms/advanced-form-elements.js') }}"></script> --}}

    <!-- Demo Js -->
    <script src="{{ asset ('adminSB/js/demo.js')}}"></script>
@endsection