@extends('layouts.adminPanel')

@section('title', __('sellsMenu.titleSellsMenu') )

@section('custom-css')
    <!-- Bootstrap Core Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset ('adminSB/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset ('adminSB/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="{{ asset ('adminSB/plugins/multi-select/css/multi-select.css')}}" rel="stylesheet">
   
    <!-- Bootstrap Select Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset ('adminSB/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset ('adminSB/css/themes/all-themes.css')}}" rel="stylesheet" />
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>{{ __('sellsMenu.titleUpdateSellsMenu') }}</h2>
            </div>

            <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ __('sellsMenu.titleUpdateSellsMenu') }}
                            </h2>
                        </div>
                            @include('partials.modals')
                        <div class="body">
                            {{-- alert --}}
                            @include('partials.alert')

                            <form action="{{ route('sells.update', [ app()->getLocale(), $sells->id ] ) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                
                                <label for="employee">{{ __('sellsMenu.LabelEmployeeName') }}</label>
                                <div class="form-group">
                                    <select class="form-control show-tick" name="id_employee" data-live-search="true" required>
                                        <option value="" class="text-center ml-auto mr-auto">-- {{ __('sellsMenu.placeholderSelectEmployee') }} -- </option>
                                        @foreach ($employees as $employee)
                                            <option class="text-center ml-auto mr-auto" value="{{ $employee->id_employee }}" > {{ $employee->first_name }} {{ $employee->last_name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <label for="item">{{ __('sellsMenu.LabelItem') }}</label>
                                <div class="form-group" id="item-form">
                                    <select class="form-control show-tick" name="item_id" id="item_id" data-live-search="true" required>
                                        <option value="" class="text-center ml-auto mr-auto">-- {{ __('sellsMenu.placeholderSelectItem') }} -- </option>
                                        @foreach ($items as $item)
                                            <option class="text-center ml-auto mr-auto" value="{{ $item->id }}" > {{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <label for="price">{{ __('sellsMenu.LabelItemPrice') }}</label>
                                <div class="input-group">
                                    <div class="form-line demo-masked-input">
                                        <input type="text" class="form-control" name="price" id="price" pattern="[\d]+" title="Please Insert Number Only" placeholder="{{ __('sellsMenu.placeholderCurrency') }}" readonly required>
                                    </div>
                                </div>
                                
                                
                                <label for="discount">{{ __('sellsMenu.LabelItemDiscount') }}</label>
                                <div class="input-group">
                                    <div class="form-line demo-masked-input">
                                        <input type="text" class="form-control" name="discount" pattern="[\d]+" title="Please Insert Number Only" placeholder="{{ __('sellsMenu.placeholderDiscount') }}">
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">{{ __('labelAction.saveButtonAction') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Vertical Layout -->

        </div>
    </div>

        
</section>
    
@endsection

@section('custom-js')
    <!-- Jquery Core Js -->
    <script src="{{ asset ('adminSB/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/node-waves/waves.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{ asset ('adminSB/js/admin.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{ asset ('adminSB/js/demo.js')}}"></script>
    <script>
        $(document).on('click', '#item-form', function(){
            // locale
            let language = '{{ config('app.locale') }}';

            let item_id  = document.getElementById('item_id').value;
            let _token   = $('meta[name="csrf-token"]').attr('content');
            let url      = "/" + language + "/get-price";

            $.ajax({
                method: "POST",
                url: url,
                data: {
                    _token: _token, 
                    item_id: item_id 
                }
            }).done(function(message) {
                $("#price").val(message.data.price);
            });
        });
        
    </script>
@endsection