@extends('layouts.adminPanel')

@section('title', 'Panel')

@section('custom-css')
    <!-- Bootstrap Core Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset ('adminSB/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset ('adminSB/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset ('adminSB/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset ('adminSB/css/themes/all-themes.css')}}" rel="stylesheet" />
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
                <h2>Tanggal: {{ date('d-m-Y') }}</h2>
            </div>
            <!-- Widgets -->
            <div class="row clearfix" id="data-trx-Realtime">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">show_chart</i>
                        </div>
                        <div class="content">
                            <div class="text" style="margin-top: 0px !important;">TOTAL TRANSAKSI HARI INI</div>
                            <div class="number" style="margin-top: 5px !important; font-size:18px;">Loading</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-teal hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">show_chart</i>
                        </div>
                        <div class="content">
                            <div class="text" style="margin-top: 0px !important;">TOTAL PROFIT HARI INI</div>
                            <div class="number" style="margin-top: 5px !important; font-size:18px;">Loading</div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box bg-blue-grey hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">person_add</i>
                        </div>
                        <div class="content">
                            <div class="text" style="margin-top: 0px !important;">TOTAL CUSTOMER HARI INI</div>
                            <div class="number" style="margin-top: 5px !important; font-size:18px;">Loading</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Widgets -->

            <div class="row clearfix" id="data-trx-Realtime">
                <!-- Bar Chart -->
                <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Bar Chart Transaksi Bulanan</h2>
                            <h2>
                                @php
                                    $today = date("Y-m-d");
                                    $startDate =  date("Y-m-").( ( date("d") - date("d") ) + 1);
                                    echo \Carbon\Carbon::parse($startDate)->format('d-m-Y')." - ".\Carbon\Carbon::parse($today)->format('d-m-Y');
                                @endphp
                            </h2>
                        </div>
                        <div class="body">
                            <canvas id="line_chart" height="80"></canvas>
                        </div>
                    </div>
                </div>
                <!-- #END# Bar Chart -->
            </div>
        </div>
    </section>
@endsection


@section('custom-js')
    <!-- Jquery Core Js -->
    <script src="{{ asset ('adminSB/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/node-waves/waves.js')}}"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/jquery-countto/jquery.countTo.js') }}"></script>

    <!-- ChartJs -->
    <script src="{{ asset ('adminSB/plugins/chartjs/Chart.bundle.js')}}"></script>


    <!-- Custom Js -->
    <script src="{{ asset ('adminSB/js/admin.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{ asset ('adminSB/js/demo.js')}}"></script>
    <script src="{{ asset ('asset/conversion/conversion.js')}}"></script>
    <script src="{{ asset ('asset/chart/chart.js')}}"></script>
    <script src="{{ asset ('asset/index.js')}}"></script>
    {{-- check chart --}}
    <script>
        let userID = "{{ Auth::user()->user_id }}";
        let statusUser = "{{ Auth::user()->status_user }}";
        
        if(statusUser == "admin"){
            let url = '{{ env('API_CHARTADMIN') }}';
            let title = "Transaksi JJ SHOP"
            barChartEmploye(url, title)
        }else{
            let url = '{{ env('API_CHARTUSER') }}' + userID;
            let title = "Transaksi " + "{{ ucwords(Auth::user()->name) }}"
            barChartUser(url, title)
        }
    </script>
    {{-- widget trx --}}
    <script>
        if(statusUser == "admin"){
            let TrxCustomerToday = '{{ env('API_TRXTODAY') }}';
            trxData(TrxCustomerToday);
        }else{
            let TrxCustomerToday = '{{ env('API_TRXUSERTODAY') }}' + userID;
            trxData(TrxCustomerToday);
        }
    </script>
    
@endsection