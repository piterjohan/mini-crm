@extends('layouts.adminPanel')

@section('title', __('employeesMenu.titleCreateEmployee') )

@section('custom-css')
    <!-- Bootstrap Core Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset ('adminSB/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset ('adminSB/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="{{ asset ('adminSB/plugins/multi-select/css/multi-select.css')}}" rel="stylesheet">
   
    <!-- Bootstrap Select Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset ('adminSB/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset ('adminSB/css/themes/all-themes.css')}}" rel="stylesheet" />
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>{{ __('employeesMenu.titleCreateEmployee') }}</h2>
            </div>

            <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Form {{ __('employeesMenu.titleCreateEmployee') }}
                            </h2>
                            <p class="m-t-2">
                                <a href="#" data-toggle="modal" data-target="#defaultModalEmployee"> {{ __('employeesMenu.LabelImportEmployee') }} </a>
                            </p>
                            <p class="text-right">
                                <a href="{{ asset('format-excel/Format-Import-Data-Employee.xlsx') }}"> {{ __('employeesMenu.LabelDownloadFormatImportEmployee') }} </a>
                            </p>
                        </div>
                            @include('partials.modals')
                        <div class="body">
                            {{-- alert --}}
                            @include('partials.alert')

                            <form action="{{ route('employees.store', app()->getLocale() ) }}" method="POST" enctype="multipart/form-data">
                                @csrf

                                <label for="id_company">{{ __('employeesMenu.LabelCompany') }}</label>
                                <div class="form-group">
                                    <select class="form-control show-tick" name="id_company" data-live-search="true" required>
                                        <option value="" class="text-center ml-auto mr-auto">-- {{ __('employeesMenu.LabelSelectCompany') }} -- </option>
                                        @foreach ($data_companies as $itemCompany)
                                            <option class="text-center ml-auto mr-auto" value="{{ $itemCompany->id_company }}" > {{ $itemCompany->name }} </option>
                                        @endforeach
                                    </select>
                                </div>

                                <label for="first_name">{{ __('employeesMenu.LabelFirstName') }}</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="first_name" id="first_name" class="form-control" placeholder="{{ __('employeesMenu.LabelFirstName') }}" required>
                                    </div>
                                </div>

                                <label for="last_name">{{ __('employeesMenu.LabelLastName') }}</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="last_name" id="last_name" class="form-control" placeholder="{{ __('employeesMenu.LabelLastName') }}" required>
                                    </div>
                                </div>

                                <label for="email">{{ __('employeesMenu.LabelEmail') }} </label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="email" name="email" id="email" class="form-control" placeholder="{{ __('employeesMenu.LabelEmail') }}" required>
                                    </div>
                                </div>

                                <label for="phone">{{ __('employeesMenu.LabelPhone') }} </label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="phone" id="phone" maxlength="13" pattern="[\d]+" title="Please Insert Number Only" class="form-control" placeholder="{{ __('employeesMenu.LabelPhone') }}" required>
                                    </div>
                                </div>

                                <label for="password">{{ __('employeesMenu.LabelPassword') }} </label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" name="password" id="password" class="form-control" placeholder="{{ __('employeesMenu.LabelPassword') }}" required>
                                    </div>
                                </div>
                                
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">{{ __('labelAction.saveButtonAction') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Vertical Layout -->

        </div>
    </div>

        
</section>
    
@endsection

@section('custom-js')
    <!-- Jquery Core Js -->
    <script src="{{ asset ('adminSB/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/node-waves/waves.js')}}"></script>

     <!-- Jquery DataTable Plugin Js -->
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{ asset ('adminSB/js/admin.js')}}"></script>
    <script src="{{ asset ('adminSB/js/pages/tables/jquery-datatable.js')}}"></script>
    <script src="{{ asset ('adminSB/js/pages/forms/advanced-form-elements.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{ asset ('adminSB/js/demo.js')}}"></script>
@endsection