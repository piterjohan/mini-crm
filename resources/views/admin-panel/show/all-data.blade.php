@extends('layouts.adminPanel')

@section('title', __('allDataMenu.titleAllData') )

@section('custom-css')
    <!-- Bootstrap Core Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset ('adminSB/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset ('adminSB/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="{{ asset ('adminSB/plugins/multi-select/css/multi-select.css')}}" rel="stylesheet">
   
    <!-- Bootstrap Select Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

     <!-- Bootstrap Material Datetime Picker Css -->
     <link href="{{ asset ('adminSB/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />

     <!-- Bootstrap DatePicker Css -->
     <link href="{{ asset ('adminSB/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" />


    <!-- Custom Css -->
    <link href="{{ asset ('adminSB/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset ('adminSB/css/themes/all-themes.css')}}" rel="stylesheet" />

    <style>
        .hoverDateRange{
            cursor: pointer;
        }
    </style>
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">

            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ __('allDataMenu.titleCompaniesCard') }}
                            </h2>
                        </div>
                        <div class="body">
                            <div class="container-fluid">
                                <div class="row">
                                    <form action="{{ route('data.searchingCompany',[app()->getLocale()]) }}" method="get">
                                        <div class="col-xs-12 col-md-3 col-lg-3">
                                            <div class="input-group">
                                                <div class="form-line">
                                                    <input type="text" name="searchingCompany" class="form-control" id="searchingCompany" placeholder="{{ __('allDataMenu.labelPlaceHolderSearching') }}">
                                                </div>
                                                <span class="input-group-addon">
                                                    <i class="material-icons">search</i>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-3 col-lg-3">
                                            <div class="input-group date" id="bs_datepicker_component_container">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="searchingCompanyByDate" id="searchingCompanyByDate" placeholder="{{ __('allDataMenu.labelPlaceHolderSearchingByDate') }}">
                                                </div>
                                                <span class="input-group-addon hoverDateRange" id="clickSearchingCompanyByDate">
                                                    <i class="material-icons">date_range</i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-5 col-lg-5">
                                            <select class="form-control show-tick" name="timezone" data-live-search="true" required>

                                                <option value="" class="text-center ml-auto mr-auto">{{ __('allDataMenu.labelplaceholderSearchTimezone') }}</option>
                                                
                                                @foreach ($list_timezone as $timezone)
                                                    <option value="{{ $timezone->timezone }}" class="text-center ml-auto mr-auto">{{ $timezone->timezone}} ( {{ $timezone->GMT }} )</option>
                                                @endforeach
                                                
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-md-1 col-lg-1">
                                            <button type="submit" class="btn btn-default waves-effect">{{ __('labelAction.searchButtonAction') }}</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="row" id="resultCompany">
                                    @isset($get_current_timezone)
                                        <div class="col-xs-12 col-md-12 col-lg-12 mt-n5">
                                            {{ __('allDataMenu.titleCurentTimezone')}}: <b>{{ $get_current_timezone }}</b>
                                        </div>
                                    @endisset
                                    @include('partials/pagination_dynamic.data-company')
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->

            <!-- Basic Examples -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ __('allDataMenu.titleEmployeesCard') }}
                            </h2>
                        </div>
                        <div class="body">
                            <div class="container-fluid">
                                <div class="row">
                                    <form action="{{ route('data.searchingEmployee',[app()->getLocale()]) }}" method="get">
                                        <div class="col-xs-12 col-md-3 col-lg-3">
                                            <div class="input-group">
                                                <div class="form-line">
                                                    <input type="text" name="searchingEmployee" class="form-control" id="searchingEmployee" placeholder="{{ __('allDataMenu.labelPlaceHolderSearching') }}">
                                                </div>  
                                                <span class="input-group-addon">
                                                    <i class="material-icons">search</i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-3 col-lg-3">
                                            <div class="input-group date" id="bs_datepicker_component_container_2">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="searchingEmployeeByDate" id="searchingEmployeeByDate" placeholder="{{ __('allDataMenu.labelPlaceHolderSearchingByDate') }}">
                                                </div>
                                                <span class="input-group-addon hoverDateRange" id="clickSearchingEmployeeByDate">
                                                    <i class="material-icons">date_range</i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-5 col-lg-5">
                                            <select class="form-control show-tick" name="timezone" data-live-search="true" required>
                                                
                                                <option value="" class="text-center ml-auto mr-auto">{{ __('allDataMenu.labelplaceholderSearchTimezone') }}</option>
                                                
                                                @foreach ($list_timezone as $timezone)
                                                    <option value="{{ $timezone->timezone }}" class="text-center ml-auto mr-auto">{{ $timezone->timezone}} ( {{ $timezone->GMT }} )</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-md-1 col-lg-1">
                                            <button type="submit" class="btn btn-default waves-effect">{{ __('labelAction.searchButtonAction') }}</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="row" id="resultEmployee">
                                    @isset($get_current_timezone)
                                        <div class="col-xs-12 col-md-12 col-lg-12 mt-n5">
                                            {{ __('allDataMenu.titleCurentTimezone')}}: <b>{{ $get_current_timezone }}</b>
                                        </div>
                                    @endisset
                                    
                                    @include('partials/pagination_dynamic.data-employee')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- #END# Basic Examples -->
        </div>
    </div>

        
</section>
    
@endsection

@section('custom-js')
    <!-- Jquery Core Js -->
    <script src="{{ asset ('adminSB/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Autosize Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/autosize/autosize.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/node-waves/waves.js')}}"></script>

    <!-- Moment Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/momentjs/moment.js') }}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

    <!-- Bootstrap Datepicker Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset ('adminSB/js/admin.js')}}"></script>
    <script src="{{ asset ('adminSB/js/pages/forms/basic-form-elements.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{ asset ('adminSB/js/demo.js')}}"></script>

@endsection