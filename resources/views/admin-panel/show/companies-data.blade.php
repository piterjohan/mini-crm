@extends('layouts.adminPanel')

@section('title', __('companiesMenu.titleCompanyData') )

@section('custom-css')
    <!-- Bootstrap Core Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset ('adminSB/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset ('adminSB/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="{{ asset ('adminSB/plugins/multi-select/css/multi-select.css')}}" rel="stylesheet">
   
    <!-- Bootstrap Select Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset ('adminSB/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset ('adminSB/css/themes/all-themes.css')}}" rel="stylesheet" />
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2> {{ __('companiesMenu.titleCompanyData') }} </h2>
            </div>

            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ __('companiesMenu.titleCompanyData') }}
                            </h2>
                        </div>
                        <div class="body">
                            @include('partials.alert')

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>{{ __('companiesMenu.tableNumber') }}</th>
                                            <th>{{ __('companiesMenu.tableidCompany') }}</th>
                                            <th>{{ __('companiesMenu.tableCompanyName') }}</th>
                                            <th>{{ __('companiesMenu.tableCompanyEmail') }}</th>
                                            <th>{{ __('companiesMenu.tableCompanyLogo') }}</th>
                                            <th>{{ __('companiesMenu.tableAction') }}</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>{{ __('companiesMenu.tableNumber') }}</th>
                                            <th>{{ __('companiesMenu.tableidCompany') }}</th>
                                            <th>{{ __('companiesMenu.tableCompanyName') }}</th>
                                            <th>{{ __('companiesMenu.tableCompanyEmail') }}</th>
                                            <th>{{ __('companiesMenu.tableCompanyLogo') }}</th>
                                            <th>{{ __('companiesMenu.tableAction') }}</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        
                                        @foreach ($company_data as $indexCompany => $itemCompany)                                            
                                            <tr>
                                                <td>{{ $indexCompany + 1}}</td>
                                                <td>{{ $itemCompany->id_company }} </td>
                                                <td>{{ $itemCompany->name }} </td>
                                                <td>{{ $itemCompany->email }} </td>
                                                <td>
                                                    <img src="{{ asset('storage/'.$itemCompany->logo)}}" alt="company logo" width="100" height="100">
                                                </td>
                                                <td>
                                                    <form action="{{ route('companies.destroy', [app()->getLocale(), $itemCompany->id_company]) }}" method="post">
                                                        @csrf
                                                        {{ method_field('DELETE') }}
                                                        <a href="{{ route('companies.edit',[ app()->getLocale(), $itemCompany->id_company]) }}" class=" btn btn-sm btn-primary">{{ __('labelAction.editButtonAction') }}</a>
                                                        <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm( '{{ __('labelAction.warningDeleteAction') }} ')">{{ __('labelAction.deleteButtonAction') }}</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach 
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </div>

        
</section>
    
@endsection

@section('custom-js')
    <!-- Jquery Core Js -->
    <script src="{{ asset ('adminSB/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/node-waves/waves.js')}}"></script>

     <!-- Jquery DataTable Plugin Js -->
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
     <script src="{{ asset ('adminSB/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{ asset ('adminSB/js/admin.js')}}"></script>
    <script src="{{ asset ('adminSB/js/pages/tables/jquery-datatable.js')}}"></script>
    <script src="{{ asset ('adminSB/js/pages/forms/advanced-form-elements.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{ asset ('adminSB/js/demo.js')}}"></script>
@endsection