@extends('layouts.adminPanel')

@section('title', __('detailSellSummary.titleDetailSellSummary') )

@section('custom-css')
    <!-- Bootstrap Core Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset ('adminSB/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset ('adminSB/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="{{ asset ('adminSB/plugins/multi-select/css/multi-select.css')}}" rel="stylesheet">
   
    <!-- Bootstrap Select Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">

     <!-- Bootstrap Material Datetime Picker Css -->
     <link href="{{ asset ('adminSB/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />

     <!-- Bootstrap DatePicker Css -->
     <link href="{{ asset ('adminSB/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" />


    <!-- Custom Css -->
    <link href="{{ asset ('adminSB/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset ('adminSB/css/themes/all-themes.css')}}" rel="stylesheet" />

    <style>
        .hoverDateRange{
            cursor: pointer;
        }
        .n-m-bottom{
            margin-bottom: 0px !important;
        }
    </style>
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">

            @include('partials.alert')

            <!-- Basic Examples -->
            <div class="row clearfix">

            {{-- table --}}             
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ __('detailSellSummary.tableDetailTitleSellSummary') }}
                            </h2>
                            <p><a href="{{ route('data.sellSummary',[ app()->getLocale()]) }}">{{ __('labelAction.backButtonAction')}}</a></p>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>{{ __('detailSellSummary.tableNumber') }}</th>
                                            <th>{{ __('detailSellSummary.tableCreatedDate') }}</th>
                                            <th>{{ __('detailSellSummary.tableItemName') }}</th>
                                            <th>{{ __('detailSellSummary.tablePrice') }}</th>
                                            <th>{{ __('detailSellSummary.tableDiscount') }}</th>
                                            <th>{{ __('detailSellSummary.tableEmployee') }}</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>{{ __('detailSellSummary.tableNumber') }}</th>
                                            <th>{{ __('detailSellSummary.tableCreatedDate') }}</th>
                                            <th>{{ __('detailSellSummary.tableItemName') }}</th>
                                            <th>{{ __('detailSellSummary.tablePrice') }}</th>
                                            <th>{{ __('detailSellSummary.tableDiscount') }}</th>
                                            <th>{{ __('detailSellSummary.tableEmployee') }}</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach ($sells_data as $index_data => $item_sell_data)
                                            <tr>
                                                <td>{{ $index_data + 1 }}</td>
                                                <td>{{ Carbon\Carbon::parse($item_sell_data->created_date)->format('d-m-Y') }}</td>
                                                <td>{{ $item_sell_data->item->name }}</td>
                                                <td>{{ number_format($item_sell_data->price,0) }}</td>
                                                <td>{{ $item_sell_data->discount }}</td>
                                                <td>{{ $item_sell_data->employee->first_name }} {{ $item_sell_data->employee->last_name }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end table --}}
        </div>
    </div>
</section>
    
@endsection

@section('custom-js')
    <!-- Jquery Core Js -->
    <script src="{{ asset ('adminSB/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Autosize Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/autosize/autosize.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/node-waves/waves.js')}}"></script>

    <!-- Moment Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/momentjs/moment.js') }}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

    <!-- Bootstrap Datepicker Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset ('adminSB/js/admin.js')}}"></script>
    <script src="{{ asset ('adminSB/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ asset ('adminSB/js/pages/forms/basic-form-elements.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{ asset ('adminSB/js/demo.js')}}"></script>

@endsection