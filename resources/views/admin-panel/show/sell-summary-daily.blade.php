@extends('layouts.adminPanel')

@section('title', __('sellSummary.titleSellSummary') )

@section('custom-css')
    <!-- Bootstrap Core Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset ('adminSB/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset ('adminSB/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="{{ asset ('adminSB/plugins/multi-select/css/multi-select.css')}}" rel="stylesheet">
   
    <!-- Bootstrap Select Css -->
    <link href="{{ asset ('adminSB/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">

     <!-- Bootstrap Material Datetime Picker Css -->
     <link href="{{ asset ('adminSB/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />

     <!-- Bootstrap DatePicker Css -->
     <link href="{{ asset ('adminSB/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" />


    <!-- Custom Css -->
    <link href="{{ asset ('adminSB/css/style.css')}}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset ('adminSB/css/themes/all-themes.css')}}" rel="stylesheet" />

    <style>
        .hoverDateRange{
            cursor: pointer;
        }
        .n-m-bottom{
            margin-bottom: 0px !important;
        }
    </style>
@endsection


@section('content')
    <section class="content">
        <div class="container-fluid">

            @include('partials.alert')

            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ __('sellSummary.titleSellSummary') }}
                            </h2>
                        </div>
                        <div class="body">
                            <div class="container-fluid">
                                <div class="row">
                                    <form action="{{ route('data.searchingSellSummary',[app()->getLocale()]) }}" method="post">
                                        @csrf
                                        <div class="col-xs-12 col-md-4 col-lg-4 n-m-bottom">
                                            <div class="input-daterange input-group" id="bs_datepicker_range_container">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="start_date" placeholder="{{ __('sellSummary.placeholderStartDate') }}" >
                                                </div>
                                                <span class="input-group-addon">to</span>
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="end_date" placeholder="{{ __('sellSummary.placeholderEndDate') }}" >
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-md-4 col-lg-4 n-m-bottom">
                                            <select class="form-control show-tick" name="employee" data-live-search="true" >

                                                <option value="" class="text-center ml-auto mr-auto">{{ __('sellSummary.labelEmployee') }}</option>
                                                
                                                @foreach ($employees as $employee)
                                                    <option value="{{ $employee->id_employee }}" class="text-center ml-auto mr-auto">{{ $employee->first_name}} {{ $employee->last_name }}</option>
                                                @endforeach
                                                
                                            </select>
                                        </div>

                                        <div class="col-xs-12 col-md-4 col-lg-4 n-m-bottom">
                                            <select class="form-control show-tick" name="company" data-live-search="true" >

                                                <option value="" class="text-center ml-auto mr-auto">{{ __('sellSummary.labelCompany') }}</option>
                                                
                                                @foreach ($companies as $company)
                                                    <option value="{{ $company->id_company }}" class="text-center ml-auto mr-auto">{{ $company->name}}</option>
                                                @endforeach
                                                
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-md-12 col-lg-12 ml-auto n-m-bottom">
                                            <button type="submit" class="btn btn-default waves-effect">{{ __('labelAction.searchButtonAction') }}</button>
                                        </div>
                                    </form>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->

            {{-- table --}}             
            @isset($sells_summary)
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ __('sellSummary.tableTitleSellSummary') }}
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>{{ __('sellSummary.tableNumber') }}</th>
                                            <th>{{ __('sellSummary.tableEmployeeName') }}</th>
                                            <th>{{ __('sellSummary.tableDate') }}</th>
                                            <th>{{ __('sellSummary.tablePriceTotal') }}</th>
                                            <th>{{ __('sellSummary.tableDiscountTotal') }}</th>
                                            <th>{{ __('sellSummary.tableTotal') }}</th>
                                            <th>{{ __('sellSummary.tableAction') }}</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>{{ __('sellSummary.tableNumber') }}</th>
                                            <th>{{ __('sellSummary.tableEmployeeName') }}</th>
                                            <th>{{ __('sellSummary.tableDate') }}</th>
                                            <th>{{ __('sellSummary.tablePriceTotal') }}</th>
                                            <th>{{ __('sellSummary.tableDiscountTotal') }}</th>
                                            <th>{{ __('sellSummary.tableTotal') }}</th>
                                            <th>{{ __('sellSummary.tableAction') }}</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>

                                        @isset($sells_summary_employee)
                                            @foreach ($sells_summary_employee as $index_summary_employee => $item_summary_employee)
                                                <tr>
                                                    <td>{{ $index_summary_employee + 1 }}</td>
                                                    <td>{{ $item_summary_employee->employee->first_name }} {{ $item_summary_employee->employee->last_name }}</td>
                                                    <td>{{ Carbon\Carbon::parse($item_summary_employee->date)->format('d-m-Y') }}</td>
                                                    <td>{{ number_format($item_summary_employee->price_total,0) }}</td>
                                                    <td>{{ number_format($item_summary_employee->discount_total,0) }}</td>
                                                    <td>{{ number_format($item_summary_employee->total,0) }}</td>
                                                    <td>
                                                        <a href="{{ route('data.detailSellSummary',[ app()->getLocale(), $item_summary_employee->id]) }}" class=" btn btn-sm btn-info">{{ __('labelAction.detailsButtonAction') }}</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endisset

                                        @isset($sells_summary_company)
                                            @php
                                                $no = 1;
                                            @endphp

                                            @foreach ($sells_summary_company->employee as $item_company)
                                                
                                                @foreach ($item_company->sellsSummary as $index => $item_summary_company)
                                                    <tr>
                                                        <td>{{ $no++ }}</td>
                                                        <td>{{ $item_company->first_name }} {{ $item_company->last_name }}</td>
                                                        <td>{{ Carbon\Carbon::parse($item_summary_company->date)->format('d-m-Y') }}</td>
                                                        <td>{{ number_format($item_summary_company->price_total,0) }}</td>
                                                        <td>{{ $item_summary_company->discount_total }}</td>
                                                        <td>{{ number_format($item_summary_company->total,0) }}</td>
                                                        <td>
                                                            <a href="{{ route('data.detailSellSummary',[ app()->getLocale(), $item_summary_company->id]) }}" class=" btn btn-sm btn-info">{{ __('labelAction.detailsButtonAction') }}</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                
                                            @endforeach
                                        @endisset
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end table --}}
            @endisset
        </div>
    </div>
</section>
    
@endsection

@section('custom-js')
    <!-- Jquery Core Js -->
    <script src="{{ asset ('adminSB/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap/js/bootstrap.js')}}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

    <!-- Autosize Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/autosize/autosize.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/node-waves/waves.js')}}"></script>

    <!-- Moment Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/momentjs/moment.js') }}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

    <!-- Bootstrap Datepicker Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset ('adminSB/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset ('adminSB/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset ('adminSB/js/admin.js')}}"></script>
    <script src="{{ asset ('adminSB/js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ asset ('adminSB/js/pages/forms/basic-form-elements.js')}}"></script>

    <!-- Demo Js -->
    <script src="{{ asset ('adminSB/js/demo.js')}}"></script>

@endsection