<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Favicon-->
    <link rel="icon" href="{{ asset ('favicon.ico" type="image/x-icon')}}">

    <title>@yield('title')</title>
    @yield('customCss')
</head>
<body class="login-page">

    @yield('content')
    
    @yield('customJS')
</body>
</html>