 <!-- Default Size Companies-->
 <div class="modal fade" id="defaultModalCompanies" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">
                    {{ __('companiesMenu.LabelImportCompany') }}
                </h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('import.companies', app()->getLocale() )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <label for="data_company">{{ __('companiesMenu.LabelImportCompany') }}</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="file" name="data_company" id="data_company" class="form-control" required>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-link waves-effect">{{ __('labelAction.saveButtonAction') }}</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">{{ __('labelAction.closeButtonAction') }}</button>
            </div>
            </form>
        </div>
    </div>
</div>

 <!-- Default Size Companies-->
 <div class="modal fade" id="defaultModalEmployee" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">
                    {{ __('employeesMenu.LabelImportEmployee') }}
                </h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('import.employee', app()->getLocale() )}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <label for="data_employee">{{ __('employeesMenu.LabelImportEmployee') }}</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="file" name="data_employee" id="data_employee" class="form-control" required>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-link waves-effect">{{ __('labelAction.saveButtonAction') }}</button>
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">{{ __('labelAction.closeButtonAction') }}</button>
            </div>
            </form>
        </div>
    </div>
</div>