@isset($companies_data)
    @foreach ($companies_data as $dataCompany)  
        <div class="col-xs-12 col-md-4 col-lg-4">
            <div class="list-group">
                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">{{ $dataCompany->name }}</h4>
                    <p class="list-group-item-text">
                        Email: {{ $dataCompany->email }}
                    </p>
                    <small>
                        @isset($dataCompany->created_at)
                            {{ __('allDataMenu.labelCreatedAt')}}<b> {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $dataCompany->created_at)->setTimezone($get_current_timezone) }}</b>
                        @endisset
                    </small>
                    <br>
                    <small>
                        {{ __('allDataMenu.labelUpdatedAt')}}
                        <b> 
                            @isset($dataCompany->updated_at)
                                {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $dataCompany->updated_at)->setTimezone($get_current_timezone) }}
                            @endisset
                        </b>
                    </small>
                    <br>
                    <p class="list-group-item-text" style="margin-left: auto;margin-right: auto;width: 50%;">
                        <img src="{{ asset('storage/'.$dataCompany->logo)}}" alt="company logo" width="100" height="100">
                    </p>
                </a>
            </div>
        </div>
    @endforeach
    {{-- center paginate --}}
    <div class="col-xs-12 col-md-12 col-lg-12 text-center">
    {{ $companies_data->links() }}
    </div>
@endisset
