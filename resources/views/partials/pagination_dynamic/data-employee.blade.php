@isset($employees_data)
    @foreach ($employees_data as $dataEmployee)  
    <div class="col-xs-12 col-md-4 col-lg-4">
        <div class="card">
            <div class="header">
                <h2>
                    {{ $dataEmployee->first_name }} {{ $dataEmployee->last_name }}
                </h2>
                <small>
                    {{ __('allDataMenu.labelCreatedAt')}}
                    @isset($dataEmployee->created_at)
                        <b> {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $dataEmployee->created_at)->setTimezone($get_current_timezone) }}</b>
                    @endisset
                </small>
                <br>
                <small>
                    {{ __('allDataMenu.labelUpdatedAt')}}
                    @isset($dataEmployee->updated_at)
                       <b> {{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $dataEmployee->updated_at)->setTimezone($get_current_timezone) }}</b>
                    @endisset
                </small>
            </div>
            <div class="body">
                <p>Email: {{ $dataEmployee->email }}</p>
                <p>{{ __('allDataMenu.ParagrafPhone') }}: {{ $dataEmployee->phone }}</p>
                <p>{{ __('allDataMenu.ParagrafCompanyName') }}: {{ $dataEmployee->company->name }}</p>
            </div>
        </div>
    </div>
    @endforeach
    {{-- center paginate --}}
    <div class="col-xs-12 col-md-12 col-lg-12 text-center">
    {{ $employees_data->links() }}
    </div>
@endisset
