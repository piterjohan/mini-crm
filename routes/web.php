<?php

use App\Jobs\SendEmailCompanies;
use App\Models\CompaniesModel;
use Illuminate\Support\Facades\Auth;
use App\Mail\NotificationCreateCompany;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect('en/login' );
});

Route::get('switchLanguage/{lang}', 'LocalizationController@switch')->name('lang.switch');

Route::group([
        'prefix' => '{locale}',
        'where' => ['locale' => '[a-zA-Z]{2}'],
        'middleware' => ['checkLangDB']
    ], function() {

    Auth::routes(['register' => false, 'reset' => false,]);
});

// if auth
Route::group([
        'prefix' => '{locale}', 
        'where' => ['locale' => '[a-zA-Z]{2}'],
        'middleware' => ['auth', 'checkjwt', 'checkLangDB']
    ], 
    function() {
        Route::resource('/companies', 'CompaniesController');
        Route::resource('/employees', 'EmployeesController');
        Route::resource('/items', 'ItemsController');
        Route::resource('/sells', 'SellsController');

        Route::get('/show-all', 'DataController@index')->name('data.index');
        Route::get('/sell-summary', 'DataController@sellSummary')->name('data.sellSummary');
        
        // filter Data
        Route::get('/searching-company', 'DataController@searchingCompany')->name('data.searchingCompany');
        Route::get('/searching-employee', 'DataController@searchingEmployee')->name('data.searchingEmployee');
        Route::post('/searching-sell-summary', 'DataController@searchingSellSummary')->name('data.searchingSellSummary');
        Route::get('/details-sell-summary/{id}', 'DataController@detailSellSummary')->name('data.detailSellSummary');

        /**
         * ajax post
        */
        Route::post('/get-price', 'DataController@getPrice')->name('data.getPrice');

        // import Data
        Route::post('/import-data-companies', 'CompaniesController@importCompanies')->name('import.companies');
        Route::post('/import-data-employee', 'EmployeesController@importEmployee')->name('import.employee');

});
