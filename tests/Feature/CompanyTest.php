<?php

namespace Tests\Feature;

use App\Models\CompaniesModel;
use App\Models\EmployeesModel;
use App\Models\LanguagesModel;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use Tests\TestCase;

class CompanyTest extends TestCase
{
    use RefreshDatabase;

    /**
     * create lang
     * @return array $decode_lang
     */
    public function createLang()
    {
        $lang = LanguagesModel::create([
            'json_format' => json_encode([
                [
                    "lang" => "en",
                    'icon' => "flag-icon-usa"
                ],
                [
                    "lang" => "id",
                    'icon' => "flag-icon-idn"
                ],
            ]),
            'created_at' => Carbon::now(),
        ]);

        $decode_lang = json_decode($lang->json_format);
        return $decode_lang;
    }

    /**
     * acting like admin
     */
    public function actingAsAdmin()
    {
        // language
        $lang = $this->createLang();
        // if use refresh database
        $user_admin = factory(User::class)->create();

        $response = $this->actingAs($user_admin)
            ->withSession(['jwt_data' => $user_admin])
            ->withSession(['lang_data' => $lang]);

        return $response;
    }

    /**
     * For modified factory
     * @param array $attribute 
     * @return array
     */
    private function factoryCompanyToArray($attribute = [])
    {
        $data_company = factory(CompaniesModel::class)->make($attribute)->toArray();
        return $data_company;
    }

    /**
     * data form company
     * @param file $photo
     * @return array with attribute name,email,logo
     */
    private function dataCompany($photo)
    {
        return [
            'name' => '5LDig',
            'email' => '5LDig@gmail.com',
            'logo' => $photo,
            'updated_by_id' => Session::get('jwt_data')->id,
        ];
    }

    /**
     * for create data factory company
     * @return factory 
     */

    private function createFactoryCompany()
    {
        $create_company = factory(CompaniesModel::class);
        return $create_company;
    }

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }

    /** @test 
     * test 1
     * test 1 until n test define how many test 
     */
    public function onlyLoggedUserCanSeeCompanyDashboard()
    {
        $locale = app()->getLocale();

        $response = $this->get($locale . '/companies')
            ->assertRedirect($locale . '/login');

        // $response->dumpSession();
    }

    /** @test 
     * test 2
     */
    public function authenticatedUserCanSeeCompanyDashboard()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();

        // if log in with jwt data,we can see companies
        $response = $this->actingAsAdmin()
            ->get($locale . '/companies');

        $response->assertOk();
    }

    /**
     * @test
     * validation company
     * 
     */
    public function validationLogoCompanyDimension()
    {
        // $this->withoutExceptionHandling();
        $locale = app()->getLocale();
        // acting we have images size < 50
        $logo_company = UploadedFile::fake()->image('avatar.jpg', '50', '50');

        // store with request
        $response = $this->actingAsAdmin()
            ->post($locale . '/companies', $this->factoryCompanyToArray(['logo' => $logo_company]));

        $response->assertSessionHasErrors('logo');
        // $response->dumpSession();
    }

    /**
     * @test
     */
    public function validationCompanyName()
    {
        $locale = app()->getLocale();


        $response = $this->actingAsAdmin()
            ->post($locale . '/companies', $this->factoryCompanyToArray(['name' => null]));

        $response->assertSessionHasErrors('name');
        // $response->dumpSession();

    }

    /**
     * @test
     */
    public function validationCompanyEmail()
    {
        $locale = app()->getLocale();

        $response = $this->actingAsAdmin()
            ->post(
                $locale . '/companies',
                $this->factoryCompanyToArray(['email' => null])
            );

        $response->assertSessionHasErrors('email');
        // $response->dumpSession();

    }

    /** @test 
     * Store Data company
     */
    public function storeDataCompany()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();
        $login_admin = $this->actingAsAdmin();
        // acting we have images
        $logo_company = UploadedFile::fake()->image('avatar.jpg', '200', '200');

        $store_data_company = $this->createFactoryCompany()
            ->make(['logo' => $logo_company, 'created_by_id' => Session::get('jwt_data')->id])
            ->toArray();
        /**
         * log in with jwt data
         * then store data Request
         */
        $response = $login_admin->post(
            $locale . '/companies',
            $store_data_company
        );

        $response->assertSessionHas('success', null);
        // $response->dumpSession();
    }

    /** @test 
     * Store Data company
     */
    public function updateDataCompany()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();
        $login_admin = $this->actingAsAdmin();
        // acting we have images
        $logo_company = UploadedFile::fake()->image('avatar.jpg', '200', '200');

        // data company create then get id for update
        $data_company = $this->createFactoryCompany()->create(['logo' => $logo_company]);


        /**
         * log in with jwt data
         * then store data Request
         */
        $response = $login_admin->PUT(
            $locale . "/companies/" . $data_company->id_company,
            $this->dataCompany($logo_company)
        );

        $response->assertSessionHas('success', null);
        // $response->dumpSession();
    }

    /** 
     * @test
     * Delete Data company can success if not relation
     * with employee
     */
    public function deleteDataCompany()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();

        // data company from faker
        $data_company = $this->createFactoryCompany()->create();
        /**
         * log in with jwt data
         * then store data Request
         */
        $response = $this->actingAsAdmin()
            ->DELETE($locale . "/companies/" . $data_company->id_company);

        $response->assertSessionHas('success', null);
        // $response->dumpSession();
    }

    /** 
     * @test
     * Cant Delete Data company if Relation with employee
     */
    public function errorDeleteDataCompanyIfRelation()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();

        // data company from faker
        $create_data_company = $this->createFactoryCompany()->create();
        // create Data Employee
        factory(EmployeesModel::class)->create(['id_company' => $create_data_company->id_company]);

        /**
         * log in with jwt data
         * then store data Request
         */
        $response = $this->actingAsAdmin()
            ->DELETE($locale . "/companies/" . $create_data_company->id_company);

        $response->assertSessionHas('error', null);
        // $response->dumpSession();
    }

    /**
     * @test
     * Test Import excel Company
     */
    public function importFileExcelCompany()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();

        Excel::fake();
        $file_excel = public_path('format-excel/Format-Import-Data-Employee.xlsx');
        $upload = new UploadedFile(
            $file_excel,
            'Format-Import-Data-Employee.xlsx',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            null,
            true
        );

        $response = $this->actingAsAdmin()
            ->post(
                $locale . "/import-data-companies",
                ['data_company' => $upload]
            );

        $response->assertSessionHas('success');
        // $response->dumpSession();
    }
}
