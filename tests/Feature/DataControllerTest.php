<?php

namespace Tests\Feature;

use App\Models\CompaniesModel;
use App\Models\EmployeesModel;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;

class DataControllerTest extends TestCase
{
    use RefreshDatabase;
    const TIMEZONE = 'Asia/Jakarta';

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }

    /**
     * acting like admin
     */
    public function actingAsAdmin()
    {
        // if use refresh database
        $user_admin = factory(User::class)->create();
        // get instance
        $lang = new CompanyTest();
        $get_lang = $lang->createLang();

        $response = $this->actingAs($user_admin)
            ->withSession(['jwt_data' => $user_admin])
            ->withSession(['lang_data' => $get_lang]);

        return $response;
    }

    /** 
     * @test
     */
    public function canSearchFilterCompanyIfLogIn()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();

        // if log in with jwt data,we can see companies
        $response = $this->actingAsAdmin()
            ->get($locale . '/searching-company');

        $response->assertOk();
    }

    /**
     * @test
     */
    public function searchingFilterCompany()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();
        $admin_login = $this->actingAsAdmin();

        $data_company = factory(CompaniesModel::class, 2)->create();

        $company_page = 1;
        $request_company = $data_company[0]->email;

        $timezone = DataControllerTest::TIMEZONE;
        $url = "/" . $locale . "/searching-company?companyPage=" . $company_page . "&searchingCompany=" . $request_company . "&searchingCompanyByDate=" . "&timezone=" . $timezone;

        $response = $admin_login->get($url);

        $response->assertSuccessful();
        $response->assertSee($request_company, $escaped = true);

        // $response->dumpSession();
        // $response->dumpHeaders();
        // $response->dump();
    }

    /**
     * @test
     */
    public function searchingFilterEmployee()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();
        $admin_login = $this->actingAsAdmin();

        $data_company = factory(CompaniesModel::class, 1)->create();
        $data_employee = factory(EmployeesModel::class, 1)->create([
            'id_company' => $data_company[0]->id_company,
            'created_by_id' => Session::get('jwt_data')->id,
        ]);

        $employee_page = 1;
        $req_employee = $data_employee[0]->email;
        $timezone = DataControllerTest::TIMEZONE;

        $url = "/" . $locale . "/searching-employee?employeePage=" . $employee_page . "&searchingEmployee=" . $req_employee . "&searchingEmployeeByDate=" . "&timezone=" . $timezone;

        $response = $admin_login->get($url);

        $response->assertSuccessful();
        $response->assertSee($req_employee, $escaped = true);

        // $response->dumpSession();
        // $response->dumpHeaders();
        // $response->dump();
    }

    /**
     * @test
     */
    public function searchingCompanyByDateWithTimeZone()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();
        $login_admin = $this->actingAsAdmin();

        $data_company = factory(CompaniesModel::class, 2)->create([
            'created_by_id' => Session::get('jwt_data')->id
        ]);

        $company_page = 1;
        $timezone = DataControllerTest::TIMEZONE;

        $url = "/" . $locale . "/searching-company?companyPage=" . $company_page . "&searchingCompany=" . "&timezone=" . $timezone;

        $response = $login_admin->get($url);

        $response->assertSuccessful();
        $response->assertViewHas(['companies_data']);
        $response->assertSee($data_company[0]->email, $escaped = true);

        // $response->dumpSession();
        // $response->dumpHeaders();
        // $response->dump();
    }

    /**
     * @test
     * searching filter employee
     */
    public function searchingFilterEmployeeWithTimeZone()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();
        $login_admin = $this->actingAsAdmin();

        $data_company = factory(CompaniesModel::class, 1)->create();
        $data_employee = factory(EmployeesModel::class, 1)->create([
            'id_company' => $data_company[0]->id_company,
            'created_by_id' => Session::get('jwt_data')->id,
        ]);

        $employee_page = 1;
        $req_employee = $data_employee[0]->email;
        $timezone = DataControllerTest::TIMEZONE;

        $url = "/" . $locale . "/searching-employee?employeePage=" . $employee_page . "&searchingEmployee=" . "&searchingEmployeeByDate=" . "&timezone=" . $timezone;

        $response = $login_admin->get($url);

        $response->assertSuccessful();
        $response->assertViewHas(['employees_data']);
        $response->assertSee($req_employee, $escaped = true);

        // $response->dumpSession();
        // $response->dumpHeaders();
        // $response->dump();
        // dd($data_employee[0]->email);
    }

    /**
     * @test
     */
    public function searchingFilterCompanyByDate()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();
        $login_admin = $this->actingAsAdmin();

        $data_company  = factory(CompaniesModel::class, 2)->create();

        $company_page = 1;
        $request_date = date('Y-m-d');
        $timezone = DataControllerTest::TIMEZONE;

        $url = "/" . $locale . "/searching-company?companyPage=" . $company_page . "&searchingCompanyByDate=" . $request_date . "&timezone=" . $timezone;

        $response = $login_admin->get($url);

        $response->assertSuccessful();
        $response->assertViewHas(['companies_data']);
        $response->assertSee($data_company[0]->email, $escaped = true);

        // $response->dumpSession();
        // $response->dumpHeaders();
        // $response->dump();
        // dd($data_employee[0]->email);
    }

    /**
     * @test
     */
    public function searchingFilterEmployeeByDate()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();

        $login_admin = $this->actingAsAdmin();

        $data_company  = factory(CompaniesModel::class, 2)->create();
        $data_employee = factory(EmployeesModel::class, 2)->create([
            'id_company' => $data_company[0]->id_company,
            'created_by_id' => Session::get('jwt_data')->id,
        ]);

        $employee_page = 1;
        $request_date = date('Y-m-d');
        $timezone = DataControllerTest::TIMEZONE;

        $url = "/" . $locale . "/searching-employee?employeePage=" . $employee_page . "&searchingEmployee=" . "&searchingEmployeeByDate=" . $request_date . "&timezone=" . $timezone;

        $response = $login_admin->get(
            $url,
            array('HTTP_X-Requested-With' => 'XMLHttpRequest')
        );

        $response->assertSuccessful();
        $response->assertViewHas(['employees_data']);
        $response->assertSee($data_employee[0]->email, $escaped = true);

        // $response->dumpSession();
        // $response->dumpHeaders();
        // $response->dump();
        // dd($data_employee[0]->email);
    }
}
