<?php

namespace Tests\Feature;

use App\User;
use App\Jobs\SendEmailCompanies;
use App\Mail\NotificationCreateCompany;
use App\Models\CompaniesModel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class EmailTest extends TestCase
{
    use RefreshDatabase;

    /**
     * acting like admin
     */
    public function actingAsAdmin()
    {
        // if use refresh database
        $user_admin = factory(User::class)->create();
        // get instance
        $lang = new CompanyTest();
        $get_lang = $lang->createLang();

        $response = $this->actingAs($user_admin)
                    ->withSession(['jwt_data' => $user_admin])
                    ->withSession(['lang_data' => $get_lang]);
                    
        return $response;
    }

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }


    /**
     * @test
     */
    public function emailCompanyTest()
    {
        $this->actingAsAdmin();

       // create_mail
       $company_data = factory(CompaniesModel::class)->create();

        $this->withoutExceptionHandling();
        Mail::fake();
        Mail::to($company_data->email)->send(new NotificationCreateCompany($company_data));
        Mail::assertSent(NotificationCreateCompany::class);
    }

    /**
     * @test
     * Email Company Test with queue / job
     */
    public function queueEmailCompanyTest()
    {
        Queue::fake();
        $this->actingAsAdmin();

        $company_data = factory(CompaniesModel::class)->create();

        $jobEmail = new SendEmailCompanies($company_data->email);
        dispatch($jobEmail)->onQueue('notificationEmailCompany');

        // Assert a specific type of job was pushed meeting the given truth test...
        Queue::assertPushed(function (SendEmailCompanies $job) use ($jobEmail) {
            return $job->email === $jobEmail->email;
        });
    }
}
