<?php

namespace Tests\Feature;

use App\Models\CompaniesModel;
use App\Models\EmployeesModel;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use Tests\TestCase;

class EmployeeTest extends TestCase
{
    use RefreshDatabase;

    /**
     * acting like admin
     */
    public function actingAsAdmin()
    {
        // if use refresh database
        $user_admin = factory(User::class)->create();
        // get instance
        $lang = new CompanyTest();
        $get_lang = $lang->createLang();

        $response = $this->actingAs($user_admin)
                    ->withSession(['jwt_data' => $user_admin])
                    ->withSession(['lang_data' => $get_lang]);
                    
        // add session for recall
        session(['jwt_data' => $user_admin ]);
                    
        return $response;
    }
    
     /**
     * For modified factory
     * @param array $attribute 
     * @return array
     */
    private function factoryEmployeeToArray ($attribute = [])
    {
        $data_employee = factory(EmployeesModel::class)->make($attribute)->toArray();
        return $data_employee;
    }

    /**
     * for create data factory employee
     * @return factory 
     */
    private function createFactoryEmployee()
    {
        $create_employee = factory(EmployeesModel::class);
        return $create_employee;
    }
    /**
     * for create data factory company
     * @return factory 
     */
    private function createFactoryCompany()
    {
        $create_company = factory(CompaniesModel::class);
        return $create_company;
    }

    
    /**
     * data form employee
     * @param String $id_company
     * @return array like id_company,first_name, last_name, email, phone
     */
    private function dataEmployee($id_company)
    {
        return [
            'id_company' => $id_company,
            'first_name' => 'D Dia',
            'last_name' => 'D Must',
            'password' => Hash::make('Yolo123'),
            'email' => 'D2d@diablo.com',
            'phone' => '087653437866',
            'updated_id' => Session::get('jwt_data')->id,
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }

    /** @test 
     * if didnt login cant see employee dashboard
    */
    public function onlyLoginUserCanSeeEmployeeDashboard()
    {
        $locale = app()->getLocale();

        $response = $this->get($locale.'/employees')
                    ->assertRedirect($locale.'/login');
        
        // $response->dumpSession();
    }

    /** 
     * @test 
     *
    */
    public function authenticatedUserCanSeeEmployeeDashboard()
    {
        $this->withoutExceptionHandling();

        $locale = app()->getLocale();   

        // if logged we can see companies
        $response = $this->actingAsAdmin()
                    ->get($locale.'/employees')
                    ->assertOk();
        
        // $response->dumpSession();
    }

    /**
     * @test
     * validation employe
     */
    public function validateCompanyId()
    {
        // $this->withoutExceptionHandling();
        $locale = app()->getLocale();   
        
        $response = $this->actingAsAdmin()
                        ->post($locale.'/employees',
                        $this->factoryEmployeeToArray(['id_company' => '' ]) 
                    );

        $response->assertSessionHasErrors('id_company');
    }

    /**
     * @test
     */
    public function validateFirstName()
    {
        // $this->withoutExceptionHandling();
        $locale = app()->getLocale();   


        $response = $this->actingAsAdmin()
                        ->post($locale.'/employees', 
                        $this->factoryEmployeeToArray(['first_name' => '' ]) 
                    );

        $response->assertSessionHasErrors('first_name');
    }

    /**
     * @test
     */
    public function validateLastName()
    {
        // $this->withoutExceptionHandling();
        $locale = app()->getLocale();   

        $response = $this->actingAsAdmin()
                        ->post($locale.'/employees', 
                        $this->factoryEmployeeToArray(['last_name' => '' ]) 
                    );

        $response->assertSessionHasErrors('last_name');
    }

    /**
     * @test
     */
    public function validateEmailEmployee()
    {
        // $this->withoutExceptionHandling();
        $locale = app()->getLocale();

        $response = $this->actingAsAdmin()
                        ->post($locale.'/employees', 
                        $this->factoryEmployeeToArray(['email' => '' ]) 
                    );

        $response->assertSessionHasErrors('email');
    }

    /**
     * @test
     */
    public function validatePhoneEmployee()
    {
        // $this->withoutExceptionHandling();
        $locale = app()->getLocale();   

        $response = $this->actingAsAdmin()
                        ->post($locale.'/employees', 
                        $this->factoryEmployeeToArray(['phone' => '' ]) 
                    );

        $response->assertSessionHasErrors('phone');
    }

    /** @test 
     * add Data Employee
    */
    public function storeDataEmployee()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();
        $login_admin = $this->actingAsAdmin();

        // need to create company cause it relation
        $create_company = $this->createFactoryCompany()->create(['created_by_id' => Session::get('jwt_data')->id]);
        $create_employee_with_id_company = $this->factoryEmployeeToArray([
                                            'id_company' => $create_company->id_company,
                                            'password' => Hash::make('yolo'),
                                            'created_by_id' => Session::get('jwt_data')->id,
                                            ]);

        $response = $login_admin->post($locale.'/employees', 
                        $create_employee_with_id_company
                    );

        $response->assertSessionHas('success', null);        
        // $response->dumpSession();
    }

    /** 
     * @test 
    */
    public function updateDataEmployee()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();
        $login_admin = $this->actingAsAdmin();

         // need to create company cause it relation
         $create_company = $this->createFactoryCompany()->create();
         $create_employee = $this->createFactoryEmployee()->create([
                            'id_company' => $create_company->id_company, 
                            ]);
    
        $response = $login_admin->PUT($locale.'/employees/'.$create_employee->id_employee,
                            $this->dataEmployee($create_company->id_company) );
        $response->assertSessionHas('success', null);        
        // $response->dumpSession();
    }

    /** 
     * @test
    */
    public function deleteDataEmployee()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale();
        $login_admin = $this->actingAsAdmin();


        // need to create company cause it relation
        $create_company = $this->createFactoryCompany()->create();
        $create_employee = $this->createFactoryEmployee()->create([
                            'id_company' => $create_company->id_company,
                            'created_by_id' => Session::get('jwt_data')->id
                           ]);

        $response = $login_admin->DELETE($locale.'/employees/'.$create_employee->id_employee);

        $response->assertSessionHas('success', null);        
        // $response->dumpSession();
    }

     /**
     * @test
     * Test Import excel Employee
     */
    public function importFileExcelEmployee()
    {
        $this->withoutExceptionHandling();
        $locale = app()->getLocale(); 
    
        Excel::fake();
        $file_excel= public_path('format-excel/Format-Import-Data-Employee.xlsx');
        $upload = new UploadedFile(
            $file_excel,
            'Format-Import-Data-Employee.xlsx',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            null,
            true
        );
        
        $response = $this->actingAsAdmin()
                    ->post($locale."/import-data-employee",
                            ['data_employee' => $upload]);

        $response->assertSessionHas('success');
        // $response->dumpSession();
    }

}
