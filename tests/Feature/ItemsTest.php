<?php

namespace Tests\Feature;

use App\Models\ItemsModel;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class ItemsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }

    /**
     * acting like admin
     */
    public function actingAsAdmin()
    {
        // language
        // get instance
        $lang = new CompanyTest();
        $get_lang = $lang->createLang();
        // if use refresh database
        $user_admin = factory(User::class)->create();

        $response = $this->actingAs($user_admin)
            ->withSession(['jwt_data' => $user_admin])
            ->withSession(['lang_data' => $get_lang]);

        return $response;
    }

    /**
     * A basic feature test example.
     *
     * @test
     */
    public function user_can_save_items()
    {
        $this->withoutExceptionHandling();

        $admin = $this->actingAsAdmin();
        $locale = app()->getLocale();

        $data_items = factory(ItemsModel::class)
            ->make()
            ->toArray();


        $response = $admin->post($locale . "/items", $data_items);

        $response->assertSessionHas('success');
    }

    /**
     * @test
     */
    public function user_can_update_item()
    {
        $this->withoutExceptionHandling();

        $admin = $this->actingAsAdmin();
        $locale = app()->getLocale();

        $data_items = factory(ItemsModel::class)
            ->create();

        $data_update = ['name' => 'Chair', 'price' => '750000'];

        $response = $admin->PUT(
            $locale . "/items/" . $data_items->id,
            $data_update
        );

        $response->assertSessionHas("success");
    }

    /**
     * @test
     */
    public function user_can_delete_item()
    {
        $this->withoutExceptionHandling();

        $admin = $this->actingAsAdmin();
        $locale = app()->getLocale();

        $data_items = factory(ItemsModel::class)->create();

        $response = $admin->DELETE($locale. "/items/" . $data_items->id);

        $response->assertSessionHas('success');
    }

}
