<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }

    /** 
     * @test 
     * test Login with jwt
    */
    public function login_user_with_jwt_redirect_to_companies()
    {
        // $this->withoutExceptionHandling();
        $locale = app()->getLocale();
        $user = factory(User::class)->create();
        // get instance
        $lang = new CompanyTest();
        $get_lang = $lang->createLang();

        // login with jwt data
        $response = $this->actingAs($user)
                         ->withSession(['jwt_data' => $user])
                         ->withSession(['lang_data' => $get_lang])
                         ->get($locale.'/companies');
        // check response  
        $response->assertOk();
    }
    
}
