<?php

namespace Tests\Feature;

use App\Models\CompaniesModel;
use App\Models\EmployeesModel;
use App\Models\ItemsModel;
use App\Models\SellsModel;
use App\Models\SellsSummaryModel;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class SellsControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }

    /**
     * acting like admin
     */
    public function actingAsAdmin()
    {
        // language
        // get instance
        $lang = new CompanyTest();
        $get_lang = $lang->createLang();
        // if use refresh database
        $user_admin = factory(User::class)->create();

        $response = $this->actingAs($user_admin)
            ->withSession(['jwt_data' => $user_admin])
            ->withSession(['lang_data' => $get_lang]);

        return $response;
    }

    /**
     * @test
     */
    public function user_can_save_sells_with_sells_summary()
    {
        $locale = app()->getLocale();
        $admin = $this->actingAsAdmin();

        // create dummy
        $data_dummy = $this->createDummyData();

        $sell = factory(SellsModel::class)->make([
            'item_id' => $data_dummy['item']->id,
            'price' => $data_dummy['item']->price,
            'discount' => (25 / 100),
            'id_employee' => $data_dummy['employee']->id_employee,
        ])->toArray();

        $response = $admin->post($locale . '/sells', $sell);
        $response->assertSessionHas('success');
    }

    /**
     * @test
     */
    public function user_can_update_sells()
    {
        $locale = app()->getLocale();
        $admin = $this->actingAsAdmin();

        // create dummy
        $data_dummy = $this->createDummyData();

        $data_sell = [
            'item_id' => $data_dummy['item']->id,
            'price' => $data_dummy['item']->price,
            'discount' => 15 / 100,
            'id_employee' => $data_dummy['employee']->id_employee,
        ];
        $response = $admin->PUT($locale . '/sells/' . $data_dummy['sell'][0]->id, $data_sell);

        $response->assertSessionHas('success');
    }

    /**
     * @test
     */
    public function user_can_delete_sells()
    {
        $locale = app()->getLocale();
        $admin = $this->actingAsAdmin();

        // create dummy
        $data_dummy = $this->createDummyData();

        $response = $admin->DELETE($locale . '/sells/' . $data_dummy['sell'][0]->id);

        $response->assertSessionHas('success');
    }

    /**
     * @return array factory $item, $company $employee $sell
     * with key item, company, employee, sell(array)
     */
    public function createDummyData()
    {
        // create dummy
        $item = factory(ItemsModel::class)->create();
        $company = factory(CompaniesModel::class)->create();
        $employee = factory(EmployeesModel::class)->create(['id_company' => $company->id_company]);

        $sell = factory(SellsModel::class,2)->create([
            'item_id' => $item->id,
            'price' => $item->price,
            'discount' => (25 / 100),
            'id_employee' => $employee->id_employee,
        ]);

        return [
            'item' => $item,
            'company' => $company, 
            'employee' => $employee,
            'sell' => $sell,
        ];
    }
}
