<?php

namespace Tests\Feature;

use App\Models\SellsSummaryModel;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class SellsSummaryTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }

    /**
     * acting like admin
     */
    public function actingAsAdmin()
    {
        // language
        // get instance
        $lang = new CompanyTest();
        $get_lang = $lang->createLang();
        // if use refresh database
        $user_admin = factory(User::class)->create();

        $response = $this->actingAs($user_admin)
            ->withSession(['jwt_data' => $user_admin])
            ->withSession(['lang_data' => $get_lang]);

        return $response;
    }


    /**
     * @test
     */
    public function filter_sells_summary_get_reject_when_employee_and_company_filled()
    {
        $locale = app()->getLocale();
        $admin  = $this->actingAsAdmin();

        $data_dummy = $this->createDataDummy();

        $data_test = $this->dataTest($data_dummy);

        $response = $admin->post($locale . "/searching-sell-summary", $data_test);
        $response->assertSessionHas('error');
    }

    /**
     * @test
     */
    public function filter_sells_summary_with_By_date()
    {
        $locale = app()->getLocale();
        $admin  = $this->actingAsAdmin();

        $data_dummy = $this->createDataDummy();

        $data_test = array_merge($this->dataTest($data_dummy), [
            'company' => null,
            'employee' => null
        ]);

        $response = $admin->post($locale . "/searching-sell-summary", $data_test);
        $response->assertSee($data_dummy['employee']->first_name, true);
    }

    /**
     * @test
     */
    public function filter_sells_summary_with_employee_without_company_and_date()
    {
        $locale = app()->getLocale();
        $admin  = $this->actingAsAdmin();

        $data_dummy = $this->createDataDummy();

        $data_test = array_merge($this->dataTest($data_dummy), [
            'start_date' => null,
            'end_date' => null,
            'company' => null,

        ]);


        $response = $admin->post($locale . "/searching-sell-summary", $data_test);
        $response->assertSee($data_dummy['employee']->first_name, true);
    }

    /**
     * @test
     */
    public function filter_sells_summary_with_company_without_employee_and_date()
    {
        $locale = app()->getLocale();
        $admin  = $this->actingAsAdmin();

        $data_dummy = $this->createDataDummy();

        $data_test = array_merge($this->dataTest($data_dummy), [
            'start_date' => null,
            'end_date' => null,
            'employee' => null,
        ]);

        $response = $admin->post($locale . "/searching-sell-summary", $data_test);

        $response->assertSee($data_dummy['employee']->first_name, true);
    }

    /**
     * data test
     * @param collection $data_dummy
     */
    protected function dataTest($data_dummy)
    {
        return [
            'start_date' => Carbon::today()->toDateString(),
            'end_date' => Carbon::today()->toDateString(),
            'employee' => $data_dummy['employee']->id_employee,
            'company' => $data_dummy['company']->id_company,
        ];
    }


    /**
     * create data dummy 
     * items, company, employee, sells(array) & sells summary
     * @return collection $data_dummy
     * with key item, company, employee, sell(array)
     */
    protected function createDataDummy()
    {
        $create_data = new SellsControllerTest();

        $data_dummy = $create_data->createDummyData();

        // have sell summary
        factory(SellsSummaryModel::class)->create([
            'id_employee' => $data_dummy['employee']->id_employee,
            'price_total' => $data_dummy['item']->price,
            'discount_total' => $data_dummy['sell'][0]->discount,
            'total' => $data_dummy['item']->price,
        ]);

        return $data_dummy;
    }
}
