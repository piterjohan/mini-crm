<?php

namespace Tests\Feature;

use App\Models\CompaniesModel;
use App\Models\EmployeesModel;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Laravel\Sanctum\Sanctum;

class SupportApiTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Event::fake();
    }

    /**
     * acting like admin
     * if wanna to use
     * @return model 
     */
    public function sanctumAdmin()
    {
        $admin = Sanctum::actingAs(
            factory(User::class)->create(),
            ['*']
        );

        return $admin;
    }
    /**
     * Generate Token via API Login
     * @return string access_token
     */
    public function getToken()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['password' => Hash::make(123)]);

        $response = $this->post(
            'api/auth/v1/login',
            [
                'email' => $user->email,
                'password' => "123"
            ]
        );
        $token = $response->original['access_token'];
        return $token;
    }

    /**
     * @test
     *
     */
    public function loginGetToken()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['password' => Hash::make(123)]);

        $response = $this->post(
            'api/auth/v1/login',
            [
                'email' => $user->email,
                'password' => "123"
            ]
        );

        $response->assertStatus(200)
            ->assertJsonPath(
                'access_token',
                $response->original['access_token']
            );
        // $response->dumpSession();
    }

    /**
     * @test
     *
     */
    public function listOfEmployeeById()
    {
        $token = $this->getToken();
        $this->withoutExceptionHandling();
        // $admin = $this->sanctumAdmin();
        $admin = factory(User::class)->create();

        // create company and employee relation
        $company = factory(CompaniesModel::class)->create(['created_by_id' => $admin->id]);
        factory(EmployeesModel::class, 2)->create([
            'id_company' => $company->id_company,
            'created_by_id' => $admin->id
        ]);

        // authentication token
        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Bearer' => $token,
        ])->json(
            'POST',
            'api/auth/v1/company-employees',
            ['company_id' => $company->id_company]
        );

        $response->assertStatus(200)
            ->assertJsonPath('data.0.company.id_company', $company->id_company);
        // $response->dumpHeaders();
        // $response->dumpSession();

    }
}
